// common header files
#include<stdio.h> // for printf, fileio
#include<stdlib.h> // for exit
#include<Windows.h> // for 3 lakh windowing functions
#include<stdbool.h>

#define STB_IMAGE_IMPLEMENTATION

// OpenGL header files
#include<gl/GL.h> // path: C:/Program files(x86)/Windows Kits/x.SDK dir/include/um/include/gl
#include<gl/GLU.h> // GLU - Graphics library utilities, not GLUT
#include "stb_image.h"

#include "OGL.h"
#include <vector>
#include "vmath.h"
#include <time.h>

// MACROS
#define WIN_WIDTH 800
#define WIN_HEIGHT 600
#define MAX_PARTICLES 300

// Link with OpenGL library
#pragma comment(lib, "OpenGL32.lib")
#pragma comment(lib, "glu32.lib")

#define Intro 0
#define HanumanFlyingTowardsSun 7
#define HanumanEatingSun 26
#define HanumanLiftingSanjivani 47
// #define LankaDahan 0
// #define Outro 33

#define LankaDahan 94
#define Outro 127

// #define HanumanEatingSun 0
// #define HanumanLiftingSanjivani 21

// global function declarations
LRESULT CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM); // WPARAM - 16 bit, LPARAM - 32 bit

// global variable declarations
HWND ghwnd = NULL;
DWORD dwStyle = 0;
WINDOWPLACEMENT wpPrev = { sizeof(WINDOWPLACEMENT) }; // best way to declare structure without initialization
BOOL gbFullScreen = FALSE;
BOOL gbActiveWindow = FALSE;
FILE* gpFile = NULL;
BOOL bDone = FALSE;

// OpenGL related global variable declarations
HDC ghdc = NULL;
HGLRC ghrc = NULL;

int Global_Time = 0;
GLfloat fTimer = 0.0f;
SYSTEMTIME times_in_sec;
int present = Intro;

float objX = 0.0f, objY = 0.0f, objZ = 0.0f, scaleX = 1.0f, scaleY = 1.0f, scaleZ = 1.0f, rotate = 0.0f;
float eyeX = 0.0f, eyeY = 0.0f, eyeZ = 0.1f;
float translateSunX = 0.30f, translateSunY = 0.0f, translateSunZ = 1.205f;
float babyHanumanFlyingX = 0.0f, babyHanumanFlyingZ = 0.0f;
float moveCameraXInGalaxy = 1.600000, moveCameraYInGalaxy = -4.399999, moveCameraZInGalaxy = 2.099999, moveCameraCenterXInGalaxy = -3.199998, moveCameraCenterYInGalaxy = 0.000000, moveCameraCenterZInGalaxy = 0.050000;
float camIncrement = 5.05f;
float centerX = 0.0f, centerY = 0.0f, centerZ = 0.0f;
float balHanumanFlyX = 0.0f;
float balHanumanFlyY = 0.0f;
float sunTranslateY = 0.0f;
float sunTranslateZ = 0.0f;
float handsMovement = 0.0f;
float hanumanEatingSceneLight = 1.0f;
float earthSceneLight = 1.0f;
float hanumanFallingCameraY = 0.0f;
float hanumanFallingCameraZ = 0.1f;
float galaxyMovingY = 0.0f;
float rotateSunToppings = 0.0f;
float demoTitleFadeInAlpha = 0.0f;
float demoTitleFadeOutAlpha = 1.0f;
float introSceneFadeInAlpha = 0.0f;
float introSceneFadeOutAlpha = 1.0f;
float outroSceneFadeInAlpha1 = 0.0f;
float outroSceneFadeInAlpha2 = 0.0f;
float outroSceneFadeInAlpha3 = 0.0f;
float outroSceneFadeInAlpha4 = 0.0f;
float outroSceneFadeInAlpha5 = 0.0f;
float outroSceneFadeOutAlpha1 = 1.0f;
float outroSceneFadeOutAlpha2 = 1.0f;
float outroSceneFadeOutAlpha3 = 1.0f;
float outroSceneFadeOutAlpha4 = 1.0f;
float outroSceneFadeOutAlpha5 = 1.0f;
float rotateBlackEarth = 0.0f;
float birdRotation = 0.0f;

GLuint Texture_DemoTitle = 0;
GLuint Texture_Intro1 = 0;

GLuint Texture_Outro = 0;
GLuint Texture_Outro1 = 0;
GLuint Texture_Outro2 = 0;
GLuint Texture_Outro3 = 0;
GLuint Texture_Outro4 = 0;
GLuint Texture_Outro5 = 0;

GLuint Texture_Galaxy = 0;
GLuint Texture_Earth = 0;
GLuint Texture_AnotherEarth = 0;
GLuint Texture_Sun = 0;
GLuint Texture_BalHanuman = 0;
GLuint Texture_BalHanumanBiceps = 0;

GLuint Texture_HanumanFace = 0;
GLuint Texture_Top_Temple1 = 0;
GLuint Texture_Base_Temple1 = 0;
GLuint Texture_Top_Temple2 = 0;
GLuint Texture_Mid_Temple2 = 0;
GLuint Texture_Base_Temple2 = 0;
GLuint Texture_Top_Temple3 = 0;
GLuint Texture_Mid_Temple3 = 0;
GLuint Texture_Base_Temple3 = 0;
GLuint Texture_Mid_Temple4 = 0;
GLuint Texture_Base_Temple4 = 0;
GLuint Texture_Top_Temple5 = 0;
GLuint Texture_Mid_Temple5 = 0;
GLuint Texture_Base_Temple5 = 0;

GLuint Texture_Fire = 0;

// 0.99f, 0.81f, 0.34f, 0.4f
float burningLankaRedLight = 0.44f;
float burningLankaGreenLight = 0.34f;
float burningLankaBlueLight = 0.22f;
float burningLankaAlphaLight = 0.4f;

GLfloat lightAmbient[] = { 0.44f, 0.34f, 0.22f, 0.4f }; // grey ambient light
GLfloat lightDiffuse[] = { 0.44f, 0.34f, 0.22f, 0.4f  }; // white diffuse light
// GLfloat lightSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat lightPosition[] = { 100.0f, 100.0f, 100.0f, 1.0f }; 

GLfloat materialAmbient[] = { 0.40f, 0.34f, 0.22f, 0.4f };
GLfloat materialDiffuse[] = { 0.40f, 0.34f, 0.22f, 0.4f  };
// GLfloat materialSpecular[] = { 1.0f, 1.0f, 1.0f, 1.0f };
GLfloat materialShininess[] = { 128.0f };

GLUquadric* quadric = NULL;
GLuint hanumanEatingScenePart = 1;

int shoulder = 0;
int elbow = 0;
int hand = 0;

GLUquadric* obj;

// Terrain variables
const int TERRAIN_MAP_X = 257;
const int TERRAIN_MAP_Z = 257;
float MAX_HEIGHT = 15;
float MAP_SCALE = 25.0;

// Variables for cubemaps
unsigned char* heightMapData = NULL;
float terrainHeightMap[TERRAIN_MAP_X][TERRAIN_MAP_Z][3];
GLuint texture_grass;

// variables for hanuman eating sun
GLuint Texture_FlyingHanuman;
GLuint Texture_IndraVajra;
GLuint Texture_SunToppings;

// variables for hanuman mountain lifting scene
GLuint Texture_SanjivaniMountain;
GLuint Texture_HanumanHand;
float cameraMovementFromMountain = 0.0f;
char *SkyTextures[] = {"nx1.png", "ny1.png", "nz1.png", "px1.png", "py1.png", "pz1.png"};

GLuint Texture_CubeMap[6];
char *Textures[] = {"nx.png", "ny.png", "nz.png", "px.png", "py.png", "pz.png"};

// Variables for mountains
double tolerance = 0.015;
double lightPos[3] = {50,35,1.95};
GLuint mytexture1 = 0;
GLuint mytexture2 = 0;

// light related variables
static GLfloat white[3] = { 1.0f, 1.0f, 1.0f };
static GLfloat yellow1[3] = { 0.91f, 0.66f, 0.0f };
static GLfloat yellow2[3] = { 0.88f, 0.80f, 0.0f };
static GLfloat yellow3[3] = { 0.93f, 0.86f, 0.0f };
static GLfloat orange[3] = { 1.0f, 0.5f, 0.0f };
static GLfloat red[3] = { 1.0f, 0.1f, 0.0f };

float posX = 0.0f;
float posY = -5.0f;
float posZ = 0.0f;

float gravX = 0.0f;
float gravY = 0.0f;
float gravZ = 0.0f;

float rotationAngle = 0.0f;

typedef struct // Create a structure for particle
{
	bool active; // Active (Yes/No)
	float life; // Particle Life
	float fade; // Fade Speed
	float r; // Red Value
	float g; // Green Value
	float b; // Blue Value
	float x; // X Position
	float y; // Y Position
	float z; // Z Position
	float xi; // X Direction
	float yi; // Y Direction
	float zi; // Z Direction
	float xg; // X Gravity
	float yg; // Y Gravity
	float zg; // Z Gravity
}
particles; // Particles Structure

// particles particle[MAX_PARTICLES]; // Particle Array (Room For Particle Info)

GLuint loop;

float xspeed; // Base X Speed (To Allow Keyboard Direction Of Tail)
float yspeed; // Base Y Speed (To Allow Keyboard Direction Of Tail)
float zoom = -40.0f;

// float coefficients1[] = { -146.0, 0.0, -1.5, 459.0, 19.0, -359.0, -23.0};
// float coefficients2[] = { 25.5, 23.5, 5.1, 39.0, 9.5, 37.0, 22.5};
// float coefficients3[] = { -40.5, 280.5, 13.0, 85.0, -568.5, 29.0, -43.5};

float coefficients1[] = { -158.0, -124.0, -1.5, 587.0, 109.0, -436.0, -23.0};
float coefficients2[] = { 70.5, 77.5, 81.09, 69.0, 79.5, 73.0, 75.5};
float coefficients3[] = { -60.5, 280.5, 250.0, 252.0, -700.5, 43.0, -43.5};

float X[] = { -158.0, -124.0, -1.5, 587.0, 109.0, -436.0, -23.0};
float Y[] = { 70.5, 77.5, 81.09, 69.0, 79.5, 73.0, 75.5};
float Z[] = { -60.5, 280.5, 250.0, 252.0, -700.5, 43.0, -43.5};

int selectedPoint = -1;

float yawPoints[] = {-90 , -100 , -140 , -180 , -220 ,  -270, -360};
float yaw = -90.0f;

float pitch = 0.0f;
float pitchPosition[] = {0 , 0 , 0};

vmath::vec3 worldUp = vmath::vec3(0.0, 1.0, 0.0);


vmath::vec3 position = vmath::vec3(0, 0, 0);

vmath::vec3 front = vmath::vec3(0.0, 0.0, -1.0);
vmath::vec3 up = vmath::vec3(0.0, 1.0, 0.0);
vmath::vec3 right;

class Fire 
{
	public:
	
	particles particle[MAX_PARTICLES];

	Fire() 
	{
		for (loop = 0; loop < MAX_PARTICLES; loop++) // Initials all the particles
		{
			particle[loop].active = true; // Make all the particles active
			particle[loop].life = 1.0f; // Give all the particles full life
			particle[loop].fade = 0.2f; // Fade speed
			particle[loop].r = 1.0f;
			particle[loop].g = 1.0f;
			particle[loop].b = 1.0f;
			particle[loop].xi = 1.0f; // Speed on X Axis
			particle[loop].yi = 1.0f; // Speed on Y Axis
			particle[loop].zi = 1.0f; // Speed on Z Axis
			particle[loop].xg = -0.5f; // Set Horizontal Pull
			particle[loop].yg = 0.8f; // Set Vertical Pull
			particle[loop].zg = 0.0f; // Set Pull On Z Axis To Zero
		}
	}

	~Fire()
	{

	}

	void display()
	{
		glDisable(GL_LIGHTING);
		for (loop = 0; loop < MAX_PARTICLES; loop++) // Loop through all the particles
		{
			if (particle[loop].active) // If the particle is active
			{
				float x = particle[loop].x; // Take particle X position
				float y = particle[loop].y; // Take particle Y position
				float z = particle[loop].z + zoom; // Particle Z pos + zoom

				//////////////////// Draw the particle using our RGB values, fade the particles based on it's life
				glColor4f(particle[loop].r, particle[loop].g, particle[loop].b, particle[loop].life);

				glBegin(GL_POLYGON);

				glVertex3f(x + 0.2f, y + 0.2f, z);
				glVertex3f(x - 0.2f, y + 0.2f, z);
				glVertex3f(x + 0.2f, y - 0.2f, z);
				glVertex3f(x - 0.2f, y - 0.2f, z);

				glEnd();

				particle[loop].x += particle[loop].xi / (750); // Move on the X axis by X speed
				particle[loop].y += particle[loop].yi / (1000); // Move on the Y axis by Y speed
				particle[loop].z += particle[loop].zi / (750); // Move on the Z axis by Z speed

				particle[loop].xi += (particle[loop].xg);
				particle[loop].yi += (particle[loop].yg);
				particle[loop].zi += (particle[loop].zg);
				particle[loop].life -= particle[loop].fade;

				if (particle[loop].life < 0.0f) // If particle is burned out
				{
					particle[loop].life = 1.0f; // Give it new life
					particle[loop].fade = (float)(rand() % 100) / 1000.0f + 0.003f; // Fade value	
					particle[loop].x = posX; // Center on x axis
					particle[loop].y = posY; // Center on y axis
					particle[loop].z = posZ; // Center on z axis
					particle[loop].xi = xspeed + (float)((rand() % 60) - 30.0f); // X axis speed and direction
					particle[loop].yi = yspeed + (float)((rand() % 60) - 30.0f); // Y axis speed and direction
					particle[loop].zi = (float)((rand() % 60) - 30.0f); // Z axis speed and direction
					particle[loop].r = white[0];
					particle[loop].g = white[1];
					particle[loop].b = white[2];
				}
				else if (particle[loop].life < 0.4f)
				{
					particle[loop].r = red[0];
					particle[loop].g = red[1];
					particle[loop].b = red[2];
				}
				else if (particle[loop].life < 0.55f)
				{
					particle[loop].r = orange[0];
					particle[loop].g = orange[1];
					particle[loop].b = orange[2];
				}
				else if (particle[loop].life < 0.65f)
				{
					particle[loop].r = yellow1[0];
					particle[loop].g = yellow1[1];
					particle[loop].b = yellow1[2];
				}
				else if (particle[loop].life < 0.75f)
				{
					particle[loop].r = yellow2[0];
					particle[loop].g = yellow2[1];
					particle[loop].b = yellow2[2];
				}
				else if (particle[loop].life < 0.85f)
				{
					particle[loop].r = yellow3[0];
					particle[loop].g = yellow3[1];
					particle[loop].b = yellow3[2];
				}
			}
		}
		glEnable(GL_LIGHTING);
	}
};

std::vector<Fire> vec;
int palaceCounter = 0;
float t = 0.0f;

// entry point function
int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpszCmdLine, int iCmdShow)
{
	// function declarations
	int initialize(void);
	void uninitialize(void);
	void display(void);
	void update(void);
    void ToggleFullScreen(void);
	
	// local variable declarations
	WNDCLASSEX wndclass;
	HWND hwnd;
	MSG msg;
	TCHAR szAppName[] = TEXT("SAMWindow");
	int iResult = 0;

	// Way 1 of centering our window
	int screenXPoint = (GetSystemMetrics(SM_CXSCREEN) / 2) - WIN_WIDTH / 2;
	int screenYPoint = (GetSystemMetrics(SM_CYSCREEN) / 2) - WIN_HEIGHT / 2;

	// Way 2 of centering our window
	//hdc = GetWindowDC(NULL);
	//int screenXPoint = (GetDeviceCaps(hdc, HORZRES) / 2) - windowWidth / 2;
	//int screenYPoint = (GetDeviceCaps(hdc, VERTRES) / 2) - windowHeight / 2;

	// code
	gpFile = fopen("Log.txt", "w");
	if (gpFile == NULL)
	{
		MessageBox(NULL, TEXT("Log.txt can't be opened.\n"), TEXT("Error"), MB_OK | MB_ICONERROR);
		exit(0);
	}

	fprintf(gpFile, "Program started successfully...\n");

	// WNDCLASSEX initialization
	wndclass.cbSize = sizeof(WNDCLASSEX); // count of bytes
	wndclass.style = CS_HREDRAW | CS_VREDRAW | CS_OWNDC;
	wndclass.cbClsExtra = 0;
	wndclass.cbWndExtra = 0;
	wndclass.lpfnWndProc = WndProc;
	wndclass.hInstance = hInstance;
	wndclass.hbrBackground = (HBRUSH)GetStockObject(BLACK_BRUSH);
	wndclass.hIcon = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));
	wndclass.hCursor = LoadCursor(NULL, IDC_ARROW);
	wndclass.lpszClassName = szAppName;
	wndclass.lpszMenuName = NULL;
	wndclass.hIconSm = LoadIcon(hInstance, MAKEINTRESOURCE(MYICON));

	// Register WNDCLASSEX
	RegisterClassEx(&wndclass);

	// create window
	hwnd = CreateWindowEx(
		WS_EX_APPWINDOW,
		szAppName,
		TEXT("Anjanichya Suta"),
		WS_OVERLAPPEDWINDOW | WS_CLIPCHILDREN | WS_CLIPSIBLINGS | WS_VISIBLE, // Because - 6 values for WS_OVERLAPPEDWINDOW, 5 not required only WM_OVERLAPPED required 
		screenXPoint,
		screenYPoint,
		WIN_WIDTH,
		WIN_HEIGHT,
		NULL,
		NULL,
		hInstance,
		NULL);

	ghwnd = hwnd;

	// initialization
	iResult = initialize();
	if (iResult != 0)
	{
		MessageBox(hwnd, TEXT("Intialize() failed\n"), TEXT("Error"), MB_OK | MB_ICONERROR);
		DestroyWindow(hwnd);
		/* OR
		uninitialize();
		exit(0);*/
	}

	// show window
	ShowWindow(hwnd, iCmdShow); // 2nd parameter another example: SW_MAXIMIZE

	SetForegroundWindow(hwnd);
	SetFocus(hwnd); // internally pass SET_FOCUS message to WndProc

	ToggleFullScreen();

	// game loop
	while (bDone == FALSE)
	{
		if (PeekMessage(&msg, NULL, 0, 0, PM_REMOVE))
		{
			if (msg.message == WM_QUIT)
				bDone = TRUE;
			else
			{
				TranslateMessage(&msg);
				DispatchMessage(&msg);
			}
		}
		else
		{
			if (gbActiveWindow == TRUE)
			{
				// Render
				display();
				// Update
				update();
			}
		}
	}

	// uninitialization
	uninitialize();

	// WPARAM is word and word is uint, so type-casted from unsigned to int
	return((int)msg.wParam); // (called only for WM_QUIT - GetMessage return value is false) msg.wParam = 0 from parameter value PostQuitMessage(WPARAM), here WPARAM is 0
}

// callback function
LRESULT CALLBACK WndProc(HWND hwnd, UINT iMsg, WPARAM wParam, LPARAM lParam)
{
	// function declarations
	void ToggleFullScreen(void);
	void resize(int, int);

	// code
	switch (iMsg)
	{
	case WM_CREATE:
		GetSystemTime(&times_in_sec);
		break;
	case WM_SETFOCUS:
		gbActiveWindow = TRUE;
		break;
	case WM_KILLFOCUS:
		gbActiveWindow = FALSE;
		break;
	case WM_SIZE:
		resize(LOWORD(lParam), HIWORD(lParam));
		break;
	case WM_ERASEBKGND:
		// Dusta kahi tari ahe mazyakde ERASEBKGND karnyasati tu WM_PAINT ch ERASEBKGND karu nakos. Ani he msg tya saglya 8 cases sati yet jya cases sati WM_PAINT msg yet
		return(0);
	case WM_KEYDOWN:
		switch (LOWORD(wParam)) {
			case VK_ESCAPE:
				DestroyWindow(hwnd);
				break;
			
			// case VK_NUMPAD1:
			// 	selectedPoint = 1;
			// break;
		
			// case VK_NUMPAD2:
			// 	selectedPoint = 2;
			// 	break;

			// case VK_NUMPAD3:
			// 	selectedPoint = 3;
			// 	break;

			// case VK_NUMPAD4:
			// 	selectedPoint = 4;
			// 	break;
			
			// case VK_NUMPAD5:
			// 	selectedPoint = 5;
			// 	break;

			// case VK_NUMPAD6:
			// 	selectedPoint = 6;
			// 	break;

			// case VK_NUMPAD7:
			// 	selectedPoint = 7;
			// 	break;
			// case VK_NUMPAD8:
			// 	selectedPoint = 8;
			//	break;
			case VK_RIGHT:
				eyeX += camIncrement;
				break;
			case VK_LEFT:
				eyeX -= camIncrement;
				break;
			case VK_UP:
				eyeY += camIncrement;
				break;
			case VK_DOWN:
				eyeY -= camIncrement;
				break;
		}
		break;
	case WM_CHAR:
		switch (LOWORD(wParam))
		{
		case 'F':
		case 'f':
			if (gbFullScreen == FALSE)
			{
				ToggleFullScreen();
				gbFullScreen = TRUE;
			}
			else
			{
				ToggleFullScreen();
				gbFullScreen = FALSE;
			}
			break;
		case 'w': 
			objY += 0.55f;
			break;
		case 's': 
			objY -= 0.55f;
			break;
		case 'a': 
			objX -= 0.55f;
			break;
		case 'q': 
			objZ -= 0.001f;
			break;
		case 'e': 
			objZ += 0.55f;
			break;
		case 'd': 
			objX += 0.55f;
			break;
		// case 'q': 
		// 	if (selectedPoint != -1) {
		// 		coefficients3[selectedPoint - 1] -= 1.0f;
		// 	}
		// 	break;
		// case 'e': 
		// 	if (selectedPoint != -1) {
		// 		coefficients3[selectedPoint - 1] += 1.0f;
		// 	}
		// 	break;
		// case 'A':
		// case 'a':
		// 	// Move the selected control point left
		// 	if (selectedPoint != -1) {
		// 		coefficients1[selectedPoint - 1] -= 1.0f;
		// 	}
		// 	break;
		// case 'D':
		// case 'd':
		// 	// Move the selected control point right
		// 	if (selectedPoint != -1) {
		// 		coefficients1[selectedPoint - 1] += 1.0f;
		// 	}
		// 	break;
		// case 'W':
		// case 'w':
		// 	// Move the selected control point up
		// 	if (selectedPoint != -1) {
		// 		coefficients2[selectedPoint - 1] += 1.0f;
		// 	}
		// 	break;
		// case 'S':
		// case 's':
		// 	// Move the selected control point down
		// 	if (selectedPoint != -1) {
		// 		coefficients2[selectedPoint - 1] -= 1.0f;
		// 	}
		// 	break;	
		case 'L':
		case 'l':
			scaleX += 0.015f;
			fprintf(gpFile, "\n\nGlobal_Time = %d\n\n", Global_Time);
			break;
		case 'J':
		case 'j':
			scaleX -= 0.015f;
			break;
		case 'I':
		case 'i':
			scaleY += 0.015f;
			break;
		case 'K':
		case 'k':
			scaleY -= 0.015f;
			break;
		case 'Y':
		case 'y':
			scaleZ -= 0.015f;
			break;
		case 'U':
		case 'u':
			scaleZ += 0.015f;
			break;
		case 'R':
		case 'r':
			rotate += 3.0f;
			break;
		case 'T':
		case 't':
			rotate -= 3.0f;
			break;
		case 'x':
			eyeZ += camIncrement;
			break;
		case 'z':
			eyeZ -= camIncrement;
			break;
		case 'v':
			centerX -= 0.05f;
			break;
		case 'n':
			centerX += 0.05f;
			break;
		case 'b':
			centerY -= 0.05f;
			break;
		case 'g':
			centerY -= 0.05f;
			break;
		case 'o':
			centerZ -= 0.05f;
			break;
		case 'p':
			centerZ += 0.05f;
			break;
		}
		break;
	case WM_CLOSE: // When window close, close the application
		DestroyWindow(hwnd);
		break;
	// Each case is respective message handler
	case WM_DESTROY: // For Window close - WndProc(Closed window handle, WM_DESTROY, 0, 0) - As window is exiting then why extra info WPARAM, LPARAM
		fprintf(gpFile, "\nobjX = %f \t objY = %f \t objZ = %f \n scaleX = %f \t scaleY = %f\t scaleZ = %f\n rotate = %f\neyeX = %f \t eyeY = %f \t eyeZ = %f\n\ncenterX = %f \t centerY = %f \t centerZ = %f\n", objX, objY, objZ, scaleX, scaleY, scaleZ, rotate, eyeX, eyeY, eyeZ, centerX, centerY, centerZ);
		fprintf(gpFile, "Last Global_Time = %d\n", Global_Time);
		PostQuitMessage(0); // WPARAM parameter passed as 0
		break;
	default:
		break;
	}
	// Default window procedure - All the messages handled or unhandled messages (350+ messages) goes to desktop window WndProc and this desktop window WndProc executes our WndProc handled messages and ignores the messages which are not handled in our WndProc
	return(DefWindowProc(hwnd, iMsg, wParam, lParam));
}

void ToggleFullScreen(void)
{
	// local variable declarations
	MONITORINFO mi = { sizeof(MONITORINFO) };

	// code
	if (gbFullScreen == FALSE)
	{
		dwStyle = GetWindowLong(ghwnd, GWL_STYLE);
		if (dwStyle & WS_OVERLAPPEDWINDOW)
		{
			if (GetWindowPlacement(ghwnd, &wpPrev) && GetMonitorInfo(MonitorFromWindow(ghwnd, MONITORINFOF_PRIMARY), &mi))
			{
				SetWindowLong(ghwnd, GWL_STYLE, dwStyle & ~WS_OVERLAPPEDWINDOW);
				SetWindowPos(ghwnd, HWND_TOP, mi.rcMonitor.left, mi.rcMonitor.top, mi.rcMonitor.right - mi.rcMonitor.left, mi.rcMonitor.bottom - mi.rcMonitor.top, SWP_NOZORDER | SWP_FRAMECHANGED); // HWND_TOP is similar to bringing WS_OVERLAPPED
			}
		}

		ShowCursor(FALSE);
	}
	else
	{
		SetWindowPlacement(ghwnd, &wpPrev);
		SetWindowLong(ghwnd, GWL_STYLE, dwStyle | WS_OVERLAPPEDWINDOW);
		SetWindowPos(ghwnd, HWND_TOP, 0, 0, 0, 0, SWP_NOMOVE | SWP_NOSIZE | SWP_NOOWNERZORDER | SWP_NOZORDER | SWP_FRAMECHANGED);
		ShowCursor(TRUE);
	}
}

int initialize(void)
{
	// function declarations
	void resize(int, int);
	BOOL LoadPNGTexture(GLuint*, char*);
	BOOL LoadHeightMapData(char*);
	void InitializeTerrainCoords();
	GLuint createTexture2D(const char *);

	PIXELFORMATDESCRIPTOR pfd;
	int iPixelFormatIndex = 0;
	ZeroMemory(&pfd, sizeof(PIXELFORMATDESCRIPTOR)); // Initialization of PIXELFORMATDESCRIPTOR with default value

	// code
	// Step 1 - initialization of PIXELFORMATDESCRIPTOR
	pfd.nSize = sizeof(PIXELFORMATDESCRIPTOR);
	pfd.nVersion = 1; // n (int) is the number of bytes
	pfd.dwFlags = PFD_DRAW_TO_WINDOW | PFD_SUPPORT_OPENGL | PFD_DOUBLEBUFFER;
	pfd.iPixelType = PFD_TYPE_RGBA;
	pfd.cColorBits = 32;
	pfd.cRedBits = 8;
	pfd.cGreenBits = 8;
	pfd.cBlueBits = 8;
	pfd.cAlphaBits = 8;
	// change 1 (Enabling depth)
	pfd.cDepthBits = 32;

	// Step 2 - Get DC
	ghdc = GetDC(ghwnd); // GetDC says "You can use this DC for anything"
	if (ghdc == NULL)
	{
		fprintf(gpFile, "GetDC failed...\n");
		return(-1);
	}

	// Step 3 - Go to OS and match the pixel formats in the OS and get the most closet or matched pixel format descriptor
	iPixelFormatIndex = ChoosePixelFormat(ghdc, &pfd);
	if (iPixelFormatIndex == 0) // Non-zero positive return value is sucess for ChoosePixelFormat() and zero is failure
	{
		fprintf(gpFile, "ChoosePixelFormat() failed...\n");
		return(-2);
	}

	// Step 4 - Set obtained pixel format
	if (SetPixelFormat(ghdc, iPixelFormatIndex, &pfd) == FALSE)
	{
		fprintf(gpFile, "SetPixelFormat() failed...\n");
		return(-3);
	}

	// Step 5 - Tell WGL(Windows graphics library) to give me OpenGL compatible DC using ghdc
	ghrc = wglCreateContext(ghdc); // wgl - bridging graphics apis, gl - only graphics apis
	if (ghrc == NULL)
	{
		fprintf(gpFile, "wglCreateContext() failed...\n");
		return(-4);
	}

	// (make rendering context current) Now ghdc will end it's role and give it to ghrc to take the control further
	if(wglMakeCurrent(ghdc, ghrc) == FALSE)
	{
		fprintf(gpFile, "wglMakeCurrent() failed...\n");
		return(-5);
	}

	// change 2 - 5 substeps - (Enabling depth)
	glShadeModel(GL_SMOOTH); // Smooth the shade of light (Beautification - optional)
	glClearDepth(1.0f); // Compulsory
	glEnable(GL_DEPTH_TEST); // Enable depth test from 8 tests of pixel forming - Compulsory
	glDepthFunc(GL_LEQUAL); // Compulsory - Pudhe je pn co-ordinates apan deu te z-cordinates shi test kar glClearDepth(1.0f) chya 1.0 sobat so te potential fragments pass kr jyanchi depth less than or equal 1.0f asel - LEQUAL - less than equal to                                                 
	glHint(GL_PERSPECTIVE_CORRECTION_HINT, GL_NICEST); // Optional - Depth madye corner objects draw kele tr te eliptical distat tr te correct kar i.e. nicest kar

	// Set the clear color of window to blue
	glClearColor(0.0f, 0.0, 0.0f, 0.0f); // Here OpenGL starts

	// light related initialization
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	// material related initialization
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	// initialize quadric
	quadric = gluNewQuadric();

	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);

	glEnable(GL_LIGHT0); // LIGHT0 is enabled by default so don't need to enable explicitely
	// glEnable(GL_LIGHTING);

	stbi_set_flip_vertically_on_load(1);

	// Texture_CubeMap[0] = createTexture2D(Textures[0]);
	// Texture_CubeMap[1] = createTexture2D(Textures[1]);
	// Texture_CubeMap[2] = createTexture2D(Textures[2]);
	// Texture_CubeMap[3] = createTexture2D(Textures[3]);
	// Texture_CubeMap[4] = createTexture2D(Textures[4]);
	// Texture_CubeMap[5] = createTexture2D(Textures[5]);

	Texture_CubeMap[0] = createTexture2D(SkyTextures[0]);
	Texture_CubeMap[1] = createTexture2D(SkyTextures[1]);
	Texture_CubeMap[2] = createTexture2D(SkyTextures[2]);
	Texture_CubeMap[3] = createTexture2D(SkyTextures[3]);
	Texture_CubeMap[4] = createTexture2D(SkyTextures[4]);
	Texture_CubeMap[5] = createTexture2D(SkyTextures[5]);

	// loading earth texture
	if (LoadPNGTexture(&Texture_Galaxy, "galaxy.png") == FALSE)
	{
		fprintf(gpFile, "Galaxy texture loading failed");
	}

	// loading earth texture
	if (LoadPNGTexture(&Texture_Earth, "world.png") == FALSE)
	{
		fprintf(gpFile, "World texture loading failed");
	}

	// loading another earth texture
	if (LoadPNGTexture(&Texture_AnotherEarth, "black_world.png") == FALSE)
	{
		fprintf(gpFile, "Another world texture loading failed");
	}

	// loading sun texture
	if (LoadPNGTexture(&Texture_Sun, "sun.png") == FALSE)
	{
		fprintf(gpFile, "Sun texture loading failed");
	}
	
	// loading balhanuman texture
	if (LoadPNGTexture(&Texture_BalHanuman, "balhanuman.png") == FALSE)
	{
		fprintf(gpFile, "BalHanuman texture loading failed");
	}

	// loading balhanuman biceps
	if (LoadPNGTexture(&Texture_BalHanumanBiceps, "biceps.png") == FALSE)
	{
		fprintf(gpFile, "BalHanuman biceps texture loading failed");
	}

	// loading balhanuman biceps
	if (LoadPNGTexture(&Texture_HanumanFace, "hanumanface.png") == FALSE)
	{
		fprintf(gpFile, "Hanuman face texture loading failed");
	}

	// loading top wall of temple1
	if (LoadPNGTexture(&Texture_Top_Temple1, "top-temple1.png") == FALSE)
	{
		fprintf(gpFile, "Top wall of temple 1 texture loading failed");
	}

	// loading base wall of temple1
	if (LoadPNGTexture(&Texture_Base_Temple1, "base-temple1.png") == FALSE)
	{
		fprintf(gpFile, "Base wall of temple 1 texture loading failed");
	}

	// loading top wall of temple2
	if (LoadPNGTexture(&Texture_Top_Temple2, "top-temple2.png") == FALSE)
	{
		fprintf(gpFile, "Top wall of temple 2 texture loading failed");
	}

	// loading mid wall of temple2
	if (LoadPNGTexture(&Texture_Mid_Temple2, "mid-temple2.png") == FALSE)
	{
		fprintf(gpFile, "Mid wall of temple 2 texture loading failed");
	}

	// loading base wall of temple2
	if (LoadPNGTexture(&Texture_Base_Temple2, "base-temple2.png") == FALSE)
	{
		fprintf(gpFile, "Base wall of temple 2 texture loading failed");
	}

	// loading top wall of temple3
	if (LoadPNGTexture(&Texture_Top_Temple3, "top-temple3.png") == FALSE)
	{
		fprintf(gpFile, "Top wall of temple 3 texture loading failed");
	}

	// loading mid wall of temple3
	if (LoadPNGTexture(&Texture_Mid_Temple3, "mid-temple3.png") == FALSE)
	{
		fprintf(gpFile, "Mid wall of temple 3 texture loading failed");
	}

	// loading base wall of temple3
	if (LoadPNGTexture(&Texture_Base_Temple3, "base-temple3.png") == FALSE)
	{
		fprintf(gpFile, "Base wall of temple 3 texture loading failed");
	}

	// loading mid wall of temple4
	if (LoadPNGTexture(&Texture_Mid_Temple4, "mid-temple4.png") == FALSE)
	{
		fprintf(gpFile, "Mid wall of temple 4 texture loading failed");
	}

	// loading base wall of temple4
	if (LoadPNGTexture(&Texture_Base_Temple4, "base-temple4.png") == FALSE)
	{
		fprintf(gpFile, "Base wall of temple 4 texture loading failed");
	}

	// loading lanka top part
	if (LoadPNGTexture(&Texture_Top_Temple5, "top-temple5.png") == FALSE)
	{
		fprintf(gpFile, "Main Lanka top texture loading failed");
	}

	// loading lanka mid part
	if (LoadPNGTexture(&Texture_Mid_Temple5, "mid-temple5.png") == FALSE)
	{
		fprintf(gpFile, "Main Lanka mid texture loading failed");
	}

	// loading lanka base part
	if (LoadPNGTexture(&Texture_Base_Temple5, "base-temple5.png") == FALSE)
	{
		fprintf(gpFile, "Main Lanka base texture loading failed");
	}

	// Terrain initialization 
	// Loading Height Map  
	LoadHeightMapData("heightMap.bmp");

	// Initializing Terrain Coordinates
	InitializeTerrainCoords();

	// Load grass
	if(LoadPNGTexture(&texture_grass, "grass.png") == FALSE)
	{
		fprintf(gpFile, "texture_grass loading failed");
	}

	// Load Sanjivani Mountain
	if(LoadPNGTexture(&Texture_SanjivaniMountain, "sanjivani_mountain.png") == FALSE)
	{
		fprintf(gpFile, "Texture_SanjivaniMountain loading failed");
	}

	// Load Hanuman Hand
	if(LoadPNGTexture(&Texture_HanumanHand, "hanuman_hand.png") == FALSE)
	{
		fprintf(gpFile, "Texture_HanumanHand loading failed");
	}

	// Load Flying Hanuman
	if(LoadPNGTexture(&Texture_FlyingHanuman, "hanuman_flying.png") == FALSE)
	{
		fprintf(gpFile, "Texture_FlyingHanuman loading failed");
	}

	// Load Indra Vajra
	if(LoadPNGTexture(&Texture_IndraVajra, "indra_vajra.png") == FALSE)
	{
		fprintf(gpFile, "Texture_IndraVajra loading failed");
	}

	// Load Sun Toppings
	if(LoadPNGTexture(&Texture_SunToppings, "sun-toppngs.png") == FALSE)
	{
		fprintf(gpFile, "Texture_SunToppings loading failed");
	}

	// Load Demo Title
	if(LoadPNGTexture(&Texture_DemoTitle, "demo_title.png") == FALSE)
	{
		fprintf(gpFile, "Texture_DemoTitle loading failed");
	}

	// Load Intro1
	if(LoadPNGTexture(&Texture_Intro1, "intro1.png") == FALSE)
	{
		fprintf(gpFile, "Texture_Intro1 loading failed");
	}

	// Load Outro1
	if(LoadPNGTexture(&Texture_Outro1, "outro1.png") == FALSE)
	{
		fprintf(gpFile, "Texture_Outro1 loading failed");
	}
	
	// Load Outro2
	if(LoadPNGTexture(&Texture_Outro2, "outro2.png") == FALSE)
	{
		fprintf(gpFile, "Texture_Outro2 loading failed");
	}

	// Load Outro3
	if(LoadPNGTexture(&Texture_Outro3, "outro3.png") == FALSE)
	{
		fprintf(gpFile, "Texture_Outro3 loading failed");
	}
	
	// Load Outro4
	if(LoadPNGTexture(&Texture_Outro4, "outro4.png") == FALSE)
	{
		fprintf(gpFile, "Texture_Outro4 loading failed");
	}

	// Load Outro5
	if(LoadPNGTexture(&Texture_Outro5, "outro5.png") == FALSE)
	{
		fprintf(gpFile, "Texture_Outro5 loading failed");
	}

	// enabling the texture
	glEnable(GL_TEXTURE_2D);

	obj = gluNewQuadric();

	PlaySound(MAKEINTRESOURCE(MYAUDIO), NULL, SND_ASYNC | SND_RESOURCE);

	resize(WIN_WIDTH, WIN_HEIGHT); // Warmup resize

	return(0);
}

void resize(int width, int height)
{
	// code
	if (height <= 0)
	{
		height = 1;
	}

	glMatrixMode(GL_PROJECTION);
	glLoadIdentity();

	gluPerspective(45.0f, (GLfloat)width / (GLfloat)height, 0.1f, 10000.0f);

	glViewport(0, 0, (GLsizei)width, (GLsizei)height); // Binacular
}

void display(void)
{
	// variable declarations
	SYSTEMTIME currentTime;

	// function declarations
	void drawHanumanLiftingSanjivaniScene();
	void drawLankaBurningScene();
	void drawCube();
	void drawCubes(float width, float height, float depth);
	void drawBalHanumanEatingSun();
	void drawBalHanumanFlyingScene();
	void colorVertices(float, float, float, float);
	void drawTemple1();
	void drawTemple2(GLuint, GLuint, GLuint);
	void drawCubes(float width, float height, float depth);
	void goldenHomesSet();
	void goldenHomesSet2();
	void goldenHomesSet3();
	void drawMainLanka();
	void drawTerrain();
	void drawLankaBurningScene();
	void drawPyramid();
	int TimeCalculation(SYSTEMTIME, SYSTEMTIME);
	float de_casteljau(float t, float coefficients[] , float size);
	float degToRad(float);

	// code
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT); // me initialize madye dilelya glColor ni maz color buffer clear color and glClearDepth madla 1.0 saglya depth buffer bits la milel ani tech potential pixel honar jyache z-coprdinte less than equal to 1.0 asel
	glMatrixMode(GL_MODELVIEW);
	glLoadIdentity();
	//srand(0);

	GetSystemTime(&currentTime);
	Global_Time = TimeCalculation(currentTime, times_in_sec);

	switch(present)
	{
		case Intro:
			glPushMatrix();
			{
				if(introSceneFadeInAlpha <= 1.0f)
				{
					introSceneFadeInAlpha += 0.008f;
					glColor4f(1.0f, 1.0f, 1.0f, introSceneFadeInAlpha);
				}

				if(Global_Time >= HanumanFlyingTowardsSun)
				{
					glColor4f(1.0f, 1.0f, 1.0f, introSceneFadeOutAlpha);
					if(introSceneFadeOutAlpha <= 0.0f)
					{
						present = HanumanFlyingTowardsSun;
					}

					introSceneFadeOutAlpha -= 0.008f;
				}

				glEnable(GL_BLEND);

				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				glBindTexture(GL_TEXTURE_2D, Texture_Intro1);

				glPushMatrix();
				{
					glTranslatef(0.000f, -0.002f, -4.5f);
					glScalef(3.325f, 1.869f, 1.0f);

					glBegin(GL_QUADS);

					// Right top
					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, 1.0f, 0.0f);

					// Left top
					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, 1.0f, 0.0f);

					// Left bottom
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, -1.0f, 0.0f);

					// Right bottom
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, -1.0f, 0.0f);

					glEnd();
				}
				glPopMatrix();
				
				glBindTexture(GL_TEXTURE_2D, 0);

				glDisable(GL_BLEND);
			}
			glPopMatrix();
			break;

		case HanumanFlyingTowardsSun:
			glPushMatrix();
			{
				drawBalHanumanFlyingScene();

				if(Global_Time >= HanumanEatingSun)
				{
					present = HanumanEatingSun;
				}
			}
			glPopMatrix();
			break;

		case HanumanEatingSun:
			glPushMatrix();
			{
				drawBalHanumanEatingSun();

				if(Global_Time >= HanumanLiftingSanjivani)
				{
					present = HanumanLiftingSanjivani;
				}
			}
			glPopMatrix();
			break;

		case HanumanLiftingSanjivani:
	// 		gluLookAt(
	// 	eyeX, eyeY, eyeZ, // eye position
	// 	0.0f, 0.0, -10.0f, // eye center
	// 	0.0f, 1.0f, 0.0f // up vector
	// );

			glPushMatrix();
			{
				drawHanumanLiftingSanjivaniScene();
				if(Global_Time >= LankaDahan)
				{
					present = LankaDahan;
				}
			}
			glPopMatrix();
			break;

		case LankaDahan:
			glPushMatrix();
			{
				glTranslatef(0.0f, 0.0f, 10.0f);
				// srand(time(NULL));

				char str[255];
				float selectedX, selectedY , selectedZ;

				selectedX = coefficients1[selectedPoint];
				selectedY = coefficients2[selectedPoint];
				selectedZ = coefficients3[selectedPoint];

				sprintf(str, "Selected Point: %d | X: %f, Y: %f Z: %f", selectedPoint + 1, selectedX, selectedY , selectedZ);
				SetWindowTextA(ghwnd, str);

				position[0] = de_casteljau(t, X, sizeof(X) / sizeof(float));
				position[1] = de_casteljau(t, Y, sizeof(Y) / sizeof(float));
				position[2] = de_casteljau(t, Z, sizeof(Z) / sizeof(float));

				yaw = de_casteljau(t, yawPoints, sizeof(yawPoints) / sizeof(float));

				/////////////////////////////////////////////////////////////////////// updating center [ YAW & PITCH ]
				vmath::vec3 front_ = vmath::vec3(
					cos(degToRad(yaw)) * cos(degToRad(pitch)),
					sin(degToRad(pitch)),
					sin(degToRad(yaw)) * cos(degToRad(pitch)));

				front = vmath::normalize(front_);

				// Recalculate right
				right = vmath::normalize(vmath::cross(front, worldUp));

				// Recalculate up
				up = vmath::normalize(vmath::cross(right, front));

				vmath::vec3 Center;
				Center = position + front;
				

				gluLookAt(
					position[0],
					position[1],
					position[2],
					
					-1,
					5.5,
					-50.5,  // camera center kuthe ahe .. apan cube camera center la ahe mhanun cube draw kelay a mhanun tyache translation ani lookat che parameters same ahe
					
					up[0],
					up[1],
					up[2]
				);

				drawLankaBurningScene();

				if(Global_Time >= Outro)
				{
					present = Outro;
				}

				// glTranslatef(0.0f, 0.0f, -5.0f);
								
				// gluLookAt(
				// 	//de_casteljau(t, coefficients1, sizeof(coefficients1) / sizeof(coefficients1[0])), de_casteljau(t, coefficients2, sizeof(coefficients1) / sizeof(coefficients1[0])), de_casteljau(t, coefficients2, sizeof(coefficients1) / sizeof(coefficients1[0])) + 10.0f,
				// 	eyeX, eyeY, eyeZ,
				// 	0.0f, 0.0f, 0.0f + 1000.0f,  // camera center kuthe ahe .. apan cube camera center la ahe mhanun cube draw kelay a mhanun tyache translation ani lookat che parameters same ahe
				// 	0.0f, 1.0f, 0.0f
				// );

				// drawLankaBurningScene();
				
				// char str[255];
				// float selectedX, selectedY, selectedZ;

				// selectedX = coefficients1[selectedPoint - 1];
				// selectedY = coefficients2[selectedPoint - 1];
				// selectedZ = coefficients3[selectedPoint - 1];
				
				// sprintf(str, "Selected Point: %d | X: %f, Y: %f, Z: %f", selectedPoint + 1, selectedX, selectedY, selectedZ);

				// SetWindowText(ghwnd, str);

				// float size = sizeof(coefficients1) / sizeof(coefficients1[0]);

				// glColor3f(1.0f, 0.0f, 0.0f); 

				// glBegin(GL_LINE_STRIP);
				// for (float t = 0.0; t <= 1.0; t += 0.01) {
				// 	float x = de_casteljau(t, coefficients1 , size);
				// 	float y = de_casteljau(t, coefficients2 , size);
				// 	float z = de_casteljau(t, coefficients3 , size);
				// 	glVertex3f(x, y, z);
				// }
				// glEnd();

				// glPointSize(2.0); ////////////////////////////////////////////////////// THIS IS DONE TO PRINT 
				// glBegin(GL_POINTS);
				// for (int i = 0; i < sizeof(coefficients1) / sizeof(coefficients1[0]); i++) {
				// 	if (i == selectedPoint) {
				// 		glColor3f(1.0f, 0.0f, 1.0f); 
				// 	}
				// 	glVertex3f(coefficients1[i], coefficients2[i], coefficients3[i]);
				// 	glColor3f(1.0f, 1.0f, 0.0f); 
				// }
				// glEnd();
			}
			glPopMatrix();
			break;

		case Outro:
			int outroTimePoint = 130;
			int outroPageDuration = 10;

			if(Global_Time <= outroTimePoint + outroPageDuration)
			{
				float fadeValue = 1.0f;
				Texture_Outro = Texture_Outro1;

				if(outroSceneFadeInAlpha1 <= 1.0f)
				{
					outroSceneFadeInAlpha1 += 0.008f;
					fadeValue = outroSceneFadeInAlpha1;
				} 
				else if(Global_Time > outroTimePoint + outroPageDuration - 2)
				{
					outroSceneFadeOutAlpha1 -= 0.008f;
					fadeValue = outroSceneFadeOutAlpha1;
				}

				glColor4f(1.0f, 1.0f, 1.0f, fadeValue);
			}
			else if(Global_Time <= outroTimePoint + outroPageDuration * 2)
			{
				float fadeValue = 1.0f;
				Texture_Outro = Texture_Outro2;

				if(outroSceneFadeInAlpha2 <= 1.0f)
				{
					outroSceneFadeInAlpha2 += 0.008f;
					fadeValue = outroSceneFadeInAlpha2;
				} 
				else if(Global_Time > (outroTimePoint + outroPageDuration * 2) - 2)
				{
					outroSceneFadeOutAlpha2 -= 0.008f;
					fadeValue = outroSceneFadeOutAlpha2;
				}

				glColor4f(1.0f, 1.0f, 1.0f, fadeValue);
			}
			else if(Global_Time <= outroTimePoint + outroPageDuration * 3)
			{
				float fadeValue = 1.0f;
				Texture_Outro = Texture_Outro3;

				if(outroSceneFadeInAlpha3 <= 1.0f)
				{
					outroSceneFadeInAlpha3 += 0.008f;
					fadeValue = outroSceneFadeInAlpha3;
				} 
				else if(Global_Time > (outroTimePoint + outroPageDuration * 3) - 2)
				{
					outroSceneFadeOutAlpha3 -= 0.008f;
					fadeValue = outroSceneFadeOutAlpha3;
				}

				glColor4f(1.0f, 1.0f, 1.0f, fadeValue);
			}
			else if(Global_Time <= outroTimePoint + outroPageDuration * 4)
			{
				float fadeValue = 1.0f;
				Texture_Outro = Texture_Outro4;

				if(outroSceneFadeInAlpha4 <= 1.0f)
				{
					outroSceneFadeInAlpha4 += 0.008f;
					fadeValue = outroSceneFadeInAlpha4;
				} 
				else if(Global_Time > (outroTimePoint + outroPageDuration * 4) - 2)
				{
					outroSceneFadeOutAlpha4 -= 0.008f;
					fadeValue = outroSceneFadeOutAlpha4;
				}

				glColor4f(1.0f, 1.0f, 1.0f, fadeValue);
			}
			else
			{
				float fadeValue = 1.0f;
				Texture_Outro = Texture_Outro5;

				if(outroSceneFadeInAlpha5 <= 1.0f)
				{
					outroSceneFadeInAlpha5 += 0.006f;
					fadeValue = outroSceneFadeInAlpha5;
				} 
				else if(Global_Time > (outroTimePoint + outroPageDuration * 5) - 2)
				{
					outroSceneFadeOutAlpha5 -= 0.008f;
					fadeValue = outroSceneFadeOutAlpha5;
				}

				glColor4f(1.0f, 1.0f, 1.0f, fadeValue);
			}

			glPushMatrix();
			{
				glEnable(GL_BLEND);

				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				glBindTexture(GL_TEXTURE_2D, Texture_Outro);

				glPushMatrix();
				{
					glTranslatef(0.000f, -0.002f, -4.5f);
					glScalef(3.325f, 1.869f, 1.0f);

					glBegin(GL_QUADS);
					// glColor3f(1.0f, 1.0f, 1.0f);
					// Right top
					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, 1.0f, 0.0f);

					// Left top
					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, 1.0f, 0.0f);

					// Left bottom
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, -1.0f, 0.0f);

					// Right bottom
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, -1.0f, 0.0f);

					glEnd();
				}
				glPopMatrix();
				
				glBindTexture(GL_TEXTURE_2D, 0);

				glDisable(GL_BLEND);
			}
			glPopMatrix();
			break;
	}

	//drawBalHanumanEatingSun();

	//glTranslatef(0.0f, 0.0f, -3.0f);

	//drawBalHanumanFlyingScene();

	// gluLookAt(eyeX, eyeY, cameraMovementFromMountain, // eye position
	// 	centerX, centerY, centerZ, // eye center
	// 	0.0f, 1.0f, 0.0f // up vector
	// );

	// glPushMatrix();
	// {
	// 	// glTranslatef(objX, objY, objZ);
	// 	// glScalef(scaleX, scaleY, scaleX);

	// 	drawPyramid();
	// }
	// glPopMatrix();

	// draw cubemap
	// glPushMatrix();
	// {
	// 	glTranslatef(1.750f, 0.0f, 0.0f); 
	// 	glPushMatrix();
	// 	{
	// 		glTranslatef(-129.25f + 126.0f, -39.00f + 40.25f, 6.250f + 28.75f);
	// 		glScalef(0.085f, 0.031f * 1.8f, 0.085f);
	// 		glRotatef(-galaxyMovingY * 2.0f, 0.0f, 1.0f, 0.0f);
	// 		drawCube();
	// 	}
	// 	glPopMatrix();

	// 	glPushMatrix();
	// 	{
	// 		// set 1
	// 		glBindTexture(GL_TEXTURE_2D, Texture_SanjivaniMountain);
	// 		gluQuadricTexture(quadric, GL_TRUE);
	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(2.50f, 0.0f, -4.00f);
	// 			glScalef(1.3f, 1.3f, 1.3f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(2.50f, 0.0f, -2.00f);
	// 			glScalef(1.5f, 1.5f, 1.5f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
						
	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(1.50f, 0.0f, -2.00f);
	// 			glScalef(1.3f, 1.3f, 1.3f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(1.50f, 0.0f, -4.00f);
	// 			glScalef(1.4f, 1.4f, 1.4f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(2.750f, -0.250f, 0.00f);
	// 			glScalef(1.3f, 1.3f, 1.3f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-0.50f, 0.0f, -2.00f);
	// 			glScalef(1.8f, 1.8f, 1.8f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-0.50f, 0.0f, -4.00f);
	// 			glScalef(1.6f, 1.6f, 1.6f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-1.50f, 0.0f, 0.0f);
	// 			glScalef(1.5f, 1.5f, 1.5f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-2.50f, 0.0f, -2.00f);
	// 			glScalef(1.3f, 1.3f, 1.3f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-2.50f, 0.0f, -4.00f);
	// 			glScalef(1.6f, 1.6f, 1.6f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-3.50f, 0.0f, 0.0f);
	// 			glScalef(1.4f, 1.4f, 1.4f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-4.50f, 0.0f, -2.00f);
	// 			glScalef(1.8f, 1.8f, 1.8f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-5.50, 0.0f, 0.0f);
	// 			glScalef(1.5f, 1.5f, 1.5f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-4.50f, 0.0f, -4.00f);
	// 			glScalef(1.2f, 1.2, 1.2);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-6.50f, 0.0f, -2.00f);
	// 			glScalef(1.4f, 1.4f, 1.4f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-6.50f, 0.0f, -4.00f);
	// 			glScalef(1.6f, 1.6f, 1.6f);

	// 			drawPyramid();
	// 		}
	// 		glPopMatrix();

	// 		// spheres

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(0.50f, 0.00f, -2.50f);     
	// 			glScalef(1.30f, 1.00f, 1.30f);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-3.00f, 0.50f, -3.00f);     
	// 			glScalef(1.30f, 1.00f, 1.00f);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-1.50f, -0.50f, -4.50f);     
	// 			glScalef(1.610f, 1.00f, 1.610f);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-3.50f, -1.00f, -0.50f);     
	// 			glScalef(0.0f, 1.00f, 0.0);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(2.00f, -1.00f, -4.25f);     
	// 			glScalef(1.91f, 1.00f, 1.91f);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(1.25f, -0.75f, 0.00f);     
	// 			glScalef(2.52f, 1.40f, 2.52f);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-4.75f, -1.00f, -4.50f);     
	// 			glScalef(1.30f, 1.05f, 1.30f);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-6.50f, -0.50f, 0.50f);     
	// 			glScalef(1.30f, 1.76f, 1.30f);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(3.00f, -0.75f, -3.750f);     
	// 			glScalef(1.61f, 1.05f, 1.00f);
	// 			gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();

	// 		// glPushMatrix();
	// 		// {
	// 		// 	glTranslatef(objX, objY, objZ);     
	// 		// 	glScalef(scaleX, scaleY, scaleX);
	// 		// 	gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
	// 		// }	
	// 		// glPopMatrix();

	// 		glPushMatrix();
	// 		{
	// 			glTranslatef(-5.00f, 0.00f, -2.00f);     
	// 			glScalef(1.610, 1.00f, 1.610);
	// 			// glTranslatef(objX, objY, 0.0f);
	// 			gluSphere(quadric, 1.0f, 8, 8); // gluSphere() create all needed normals
	// 		}	
	// 		glPopMatrix();
	// 		glBindTexture(GL_TEXTURE_2D, 0);	
	// 	}	
	// 	glPopMatrix();


	// 	glEnable(GL_BLEND);

	// 	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	// 	glBindTexture(GL_TEXTURE_2D, Texture_HanumanHand);

	// 	glPushMatrix();
	// 	{
	// 		glTranslatef(-0.250f, -7.75f, -0.50f);
	// 		glScalef(6.48f, 12.67f, 0);
	// 		glBegin(GL_QUADS);
	// 		glColor3f(1.0f, 1.0f, 1.0f);
	// 		// Right top
	// 		glTexCoord2f(1.0f, 1.0f);
	// 		glVertex3f(1.0f, 1.0f, 0.0f);

	// 		// Left top
	// 		glTexCoord2f(0.0f, 1.0f);
	// 		glVertex3f(-1.0f, 1.0f, 0.0f);

	// 		// Left bottom
	// 		glTexCoord2f(0.0f, 0.0f);
	// 		glVertex3f(-1.0f, -1.0f, 0.0f);

	// 		// Right bottom
	// 		glTexCoord2f(1.0f, 0.0f);
	// 		glVertex3f(1.0f, -1.0f, 0.0f);

	// 		glEnd();
	// 	}
	// 	glPopMatrix();
	// 	glBindTexture(GL_TEXTURE_2D, 0);

	// 	glDisable(GL_BLEND);
	// }
	// glPopMatrix();

	// glTranslatef(0.0f, 0.0f, -8.0f); // Not needed

	// drawLankaBurningScene();

	SwapBuffers(ghdc); // Win32 function, NOT OpenGL funtion
}

void drawBird(float angle) {
	void colorVertices(float, float, float, float);
	float float_rand(float, float);

	glPushMatrix();
	{
		glRotatef((birdRotation + rand() % 360) * (float_rand(0.5f, 1.0f)), 0.0f, 1.0f, 0.0f);
		glTranslatef(angle, 0.0f, 0.0f);
	
		glPushMatrix();
		{
			glRotatef(155.0f, 1.0f, 0.0f, 0.0f);
			glPushMatrix();
			{

				// glTranslatef(0, 0, -9.85f);
				// glScalef(1.35f * scaleX, 0.43f * scaleY, 1.35f * scaleX);
				glRotatef(60.0f, 1.0f, 0.0f, 0.0f);
				colorVertices(53, 30, 14, 0);
				gluCylinder(obj, 1, 0, 8, 30, 30);
				// drawHanumanLiftingSanjivaniScene();
				// if(Global_Time >= LankaDahan)
				// {
				// 	present = LankaDahan;
				// }
			}
			glPopMatrix();

			glPushMatrix();
			{

				// glTranslatef(0.50f, -0.40f,-9.85f + 0.50f);
				// glScalef(1.35f, 0.43f, 1.35f);
				glRotatef(180.0f, 0.0f, 1.0f, 0.0f);
				
				colorVertices(53, 30, 14, 0);
				gluCylinder(obj, 1, 0, 8, 30, 30);
				// drawHanumanLiftingSanjivaniScene();
				// if(Global_Time >= LankaDahan)
				// {
				// 	present = LankaDahan;
				// }
			}
			glPopMatrix();
		}
		glPopMatrix();
			
	}
	glPopMatrix();
}

float float_rand(float min, float max)
{
    float scale = rand() / (float) RAND_MAX; /* [0, 1.0] */
    return min + scale * ( max - min );      /* [min, max] */
}

void drawHanumanLiftingSanjivaniScene()
{
	// function declarations
	void drawPyramid();
	void drawCube();
	void drawBird(float);
	void colorVertices(float, float, float, float);

	// code
	glTranslatef(0.0f, 0.0f, -3.0f);
	srand(0);

	//drawBalHanumanFlyingScene();

	gluLookAt(0.0f, 0.0f, cameraMovementFromMountain, // eye position
		0.0f, 0.0f, 0.0f, // eye center
		0.0f, 1.0f, 0.0f // up vector
	);

	// glPushMatrix();
	// {
	// 	// glTranslatef(objX, objY, objZ);
	// 	// glScalef(scaleX, scaleY, scaleX);

	// 	drawPyramid();
	// }
	// glPopMatrix();

	// draw cubemap
	glPushMatrix();
	{
		glDisable(GL_LIGHTING);
		glTranslatef(1.750f, 0.0f, 0.0f); 
		glPushMatrix();
		{
			glTranslatef(-129.25f + 126.0f, -39.00f + 40.25f, 6.250f + 28.75f);
			glScalef(0.085f, 0.031f * 1.8f, 0.085f);
			glRotatef(-galaxyMovingY * 2.0f, 0.0f, 1.0f, 0.0f);
			drawCube();
		}
		glPopMatrix();

		// flying birds
		glPushMatrix();
		{
			glTranslatef(-0.30f * 20, 1.25f, 0.00f * 20);     
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(50.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-0.30f * 20, 0.90f, 0.00f * 20);     
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(60.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-0.30f * 20, 1.50f, -0.30f * 20);     
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(70.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-0.20f * 10, 0.75f, 0.15f * 10);     
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(55.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-0.15f * 10, 1.50f, -0.30f * 20);  
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(90.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.25f * 10, 1.50f, -0.30f * 20);  
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(70.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.10f * 10, 0.95f, 0.00f * 10);     
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(80.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.25f * 10, 0.80f, 0.00f * 10);     
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(60.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.05f * 10, 1.45f, -0.80f * 10);     
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(90.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.05f * 10, 0.35f, 0.15f * 10);     
			glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
			drawBird(70.0f);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-0.70f, 0.00f, -0.65f);

			// flying birds
			glPushMatrix();
			{
				glTranslatef(-0.30f * 20, 1.25f, 0.00f * 20);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(50.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.30f * 20, 0.90f, 0.00f * 20);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(60.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.30f * 20, 1.50f, -0.30f * 20);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(70.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.20f * 10, 0.75f, 0.15f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(55.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.15f * 10, 1.50f, -0.30f * 20);  
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(90.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.25f * 10, 1.50f, -0.30f * 20);  
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(70.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.10f * 10, 0.95f, 0.00f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(80.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.25f * 10, 0.80f, 0.00f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(60.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.05f * 10, 1.45f, -0.80f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(90.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.05f * 10, 0.35f, 0.15f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(70.0f);
			}
			glPopMatrix();
		}
		glPopMatrix();


		glPushMatrix();
		{
			glTranslatef(0.70f, -0.05f, 0.00f);
			
			// flying birds
			glPushMatrix();
			{
				glTranslatef(-0.30f * 20, 1.25f, 0.00f * 20);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(50.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.30f * 20, 0.90f, 0.00f * 20);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(60.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.30f * 20, 1.50f, -0.30f * 20);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(70.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.20f * 10, 0.75f, 0.15f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(55.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.15f * 10, 1.50f, -0.30f * 20);  
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(90.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.25f * 10, 1.50f, -0.30f * 20);  
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(70.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.10f * 10, 0.95f, 0.00f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(80.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.25f * 10, 0.80f, 0.00f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(60.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.05f * 10, 1.45f, -0.80f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(90.0f);
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(0.05f * 10, 0.35f, 0.15f * 10);     
				glScalef(-0.0049f + 0.010f, 0.01f + -0.0049, -0.0049f + 0.010f);
				drawBird(70.0f);
			}
			glPopMatrix();
		}
		glPopMatrix();

		glPushMatrix();
		{
			// set 1
			glBindTexture(GL_TEXTURE_2D, Texture_SanjivaniMountain);
			gluQuadricTexture(quadric, GL_TRUE);
			glPushMatrix();
			{
				colorVertices(255, 255, 255, 0);
				glTranslatef(2.50f, 0.0f, -4.00f);
				glScalef(1.3f, 1.3f, 1.3f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(2.50f, 0.0f, -2.00f);
				glScalef(1.5f, 1.5f, 1.5f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
						
				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(1.50f, 0.0f, -2.00f);
				glScalef(1.3f, 1.3f, 1.3f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(1.50f, 0.0f, -4.00f);
				glScalef(1.4f, 1.4f, 1.4f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(2.750f, -0.250f, 0.00f);
				glScalef(1.3f, 1.3f, 1.3f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.50f, 0.0f, -2.00f);
				glScalef(1.8f, 1.8f, 1.8f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.50f, 0.0f, -4.00f);
				glScalef(1.6f, 1.6f, 1.6f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-1.50f, 0.0f, 0.0f);
				glScalef(1.5f, 1.5f, 1.5f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-2.50f, 0.0f, -2.00f);
				glScalef(1.3f, 1.3f, 1.3f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-2.50f, 0.0f, -4.00f);
				glScalef(1.6f, 1.6f, 1.6f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-3.50f, 0.0f, 0.0f);
				glScalef(1.4f, 1.4f, 1.4f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-4.50f, 0.0f, -2.00f);
				glScalef(1.8f, 1.8f, 1.8f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-5.50, 0.0f, 0.0f);
				glScalef(1.5f, 1.5f, 1.5f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-4.50f, 0.0f, -4.00f);
				glScalef(1.2f, 1.2, 1.2);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-6.50f, 0.0f, -2.00f);
				glScalef(1.4f, 1.4f, 1.4f);

				drawPyramid();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-6.50f, 0.0f, -4.00f);
				glScalef(1.6f, 1.6f, 1.6f);

				drawPyramid();
			}
			glPopMatrix();

			// spheres

			glPushMatrix();
			{
				glTranslatef(0.50f, 0.00f, -2.50f);     
				glScalef(1.30f, 1.00f, 1.30f);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-3.00f, 0.50f, -3.00f);     
				glScalef(1.30f, 1.00f, 1.00f);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-1.50f, -0.50f, -4.50f);     
				glScalef(1.610f, 1.00f, 1.610f);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-3.50f, -1.00f, -0.50f);     
				glScalef(0.0f, 1.00f, 0.0);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(2.00f, -1.00f, -4.25f);     
				glScalef(1.91f, 1.00f, 1.91f);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(1.25f, -0.75f, 0.00f);     
				glScalef(2.52f, 1.40f, 2.52f);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-4.75f, -1.00f, -4.50f);     
				glScalef(1.30f, 1.05f, 1.30f);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-6.50f, -0.50f, 0.50f);     
				glScalef(1.30f, 1.76f, 1.30f);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(3.00f, -0.75f, -3.750f);     
				glScalef(1.61f, 1.05f, 1.00f);
				gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();

			// glPushMatrix();
			// {
			// 	glTranslatef(objX, objY, objZ);     
			// 	glScalef(scaleX, scaleY, scaleX);
			// 	gluSphere(quadric, 1.0f, 5, 8); // gluSphere() create all needed normals
			// }	
			// glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-5.00f, 0.00f, -2.00f);     
				glScalef(1.610, 1.00f, 1.610);
				// glTranslatef(objX, objY, 0.0f);
				gluSphere(quadric, 1.0f, 8, 8); // gluSphere() create all needed normals
			}	
			glPopMatrix();
			glBindTexture(GL_TEXTURE_2D, 0);	
		}	
		glPopMatrix();


		glEnable(GL_BLEND);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindTexture(GL_TEXTURE_2D, Texture_HanumanHand);

		glPushMatrix();
		{
			glTranslatef(-0.250f + 1.10f, -7.75f + 4.95f, -0.50f + -2.20f);
			glScalef(6.48f + 1.36f, 12.67f + -4.55f, 0);
			glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);
			// Right top
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			// Left top
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			// Left bottom
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			// Right bottom
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glEnd();
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		glDisable(GL_BLEND);
	}
	glPopMatrix();
}

void drawLankaBurningScene()
{
	// function decalarations
	void drawCube();
	void drawTemple1();
	void drawTemple2(GLuint, GLuint, GLuint);
	void drawCubes(float width, float height, float depth);
	void goldenHomesSet();
	void goldenHomesSet2();
	void goldenHomesSet3();
	void drawMainLanka();
	void drawTerrain();
	float de_casteljau(float, float*, int);

	// code
	srand((unsigned int)GetTickCount());

	lightAmbient[0] = burningLankaRedLight;
	lightAmbient[1] = burningLankaGreenLight;
	lightAmbient[2] = burningLankaBlueLight;
	lightAmbient[3] = burningLankaAlphaLight;

	lightAmbient[0] = burningLankaRedLight;
	lightDiffuse[1] = burningLankaGreenLight;
	lightDiffuse[2] = burningLankaBlueLight;
	lightDiffuse[3] = burningLankaAlphaLight;

	materialAmbient[0] = burningLankaRedLight;
	materialAmbient[1] = burningLankaGreenLight;
	materialAmbient[2] = burningLankaBlueLight;
	materialAmbient[3] = burningLankaAlphaLight;

	materialDiffuse[0] = burningLankaRedLight;
	materialDiffuse[1] = burningLankaGreenLight;
	materialDiffuse[2] = burningLankaBlueLight;
	materialDiffuse[3] = burningLankaAlphaLight;
	
	// light related initialization
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	// material related initialization
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	// float x[] = { -10.0, 10.4, 10.5, 20.6, -10.8, -10.85, 1.0 };
	// float y[] = { 0.0, 2.6, 3.8, -0.4, -3.1, 4.2, 2.95 };
	// float z[] = { -0.5, -0.5, 0.0, 0.2, 0.0, 0.0, 0.0 };
	// int arrSize = sizeof(x) / sizeof(float);

	//glTranslatef(-46.74, -60.59, -13.55-2.45 + 15.600033f);

	// gluLookAt(
	// 	de_casteljau(t, x, arrSize), de_casteljau(t, y, arrSize), de_casteljau(t, y, arrSize), // eye position
	// 	centerX, centerY, centerZ, // eye center
	// 	0.0f, 1.0f, 0.0f // up vector
	// );

	// gluLookAt(
	// 	-18.25-46.74 + de_casteljau(t, x, arrSize), 2.449 +81.59+ de_casteljau(t, y, arrSize), -13.55 + de_casteljau(t, y, arrSize)-2.45 + 15.600033f, // eye position
	// 	centerX, centerY, centerZ, // eye center
	// 	0.0f, 1.0f, 0.0f // up vector
	// );

	// gluLookAt(
	// 	-18.25-46.74 + eyeX, 2.449 +81.59 + eyeY, -13.55-2.45 + 15.600033f + eyeZ, // eye position
	// 	centerX, centerY, centerZ, // eye center
	// 	0.0f, 1.0f, 0.0f // up vector
	// );

	// for (int i = 0; i < sizeof(x) / sizeof(float); i++)
	// {
	// 	glBegin(GL_POINTS);
	// 	glVertex3f(x[i], y[i], 0.0f);
	// 	glEnd();
	// }

	// lanka burning

	// Main temple Fire
	glPushMatrix();	
	{
		glTranslatef(30.74f, 21.0f, 36.89f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f, 21.0f, 7.50f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f, 21.0f, 7.50f + 15.0f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f, 21.0f + -9.50f, 7.50f + 23.0f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f, 21.0f + -9.50f, 7.50f + 34.50f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f, 21.0f, 7.50f + 42.50f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f, 21.0f + -3.50f, 7.50f + 55.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f + 3.50f, 21.0f + -4.50f, 7.50f + 75.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f + 19.00f, 21.0f + -4.50f, 7.50f + 75.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f + 29.00f, 21.0f + -5.00f, 7.50f + 75.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f + 46.00f, 21.0f, 7.50f + 75.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(10.0f + 54.50f, 21.0f + -5.00f, 7.50f + 75.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f, 33.00f, 59.500f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + 16.00f, 33.00f, 59.500f + -13.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + -12.50f, 33.00f + -0.50f, 59.500f + -23.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + 15.00f, 33.00f + 13.50f, 59.500f + -23.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + 31.50f, 33.00f + -17.50f, 59.500f + 10.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + 29.00f, 33.00f + -9.50f, 59.500f + -9.0f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + -14.00f, 33.00f + -16.500f, 59.500f + -52.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + -3.00f, 33.00f + -16.00f, 59.500f + -52.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + 10.50f, 33.00f + -13.00f, 59.500f + -52.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + 26.0f, 33.00f + -12.50f, 59.500f + -52.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + 31.50f, 33.00f + -10.00f, 59.500f + -36.00f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();

	glPushMatrix();	
	{
		glTranslatef(42.50f + 16.5f, 33.00f + 4.00f, 59.500f + -29.50f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();
	// glBindTexture(GL_TEXTURE_2D, 0);

	// draw terrain
	glPushMatrix();
	{
		glEnable(GL_LIGHTING);
		glTranslatef(-200.0f + 32.32f, -509.04f, -10.0f + 247.44f);
		glBindTexture(GL_TEXTURE_2D, texture_grass);
		glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		drawTerrain();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();

	// draw cubemap
	glPushMatrix();
	{
		glTranslatef(0.00f, 25.01f, 0.00f);
		glScalef(0.085f, 0.031f, 0.085f);
		drawCube();
	}
	glPopMatrix();

	// draw mountains
	// glPushMatrix();
	// {
	// 	glTranslatef(-10.05f, -2.01f, 8.04f);
	// 	glScalef(1.305f, 1.357f, 1.305f);

	// 	glTranslatef(0.9 + -64.299591, 0.1 - 4.999998 -0.3, -0.8 + -2.899999 + -51.699783);
	// 	glScalef(1.799999 * 3.999997 * 22.940430, 1.44 * 2.879998 * 14.270232 * 0.910000, 1.909999 * 14.380235);
	// 	glRotatef(25, 1, 0, 0);
	// 	glRotatef(-53, 0.0f, 1.0f, 0.0f);
	// 	fracMountain(-0.85, 0.0, -1.0, -0.85, 0.0, 0.0, 0.85, 0.0, 0.0, 0.85, 0.0, -1.0);
	// }
	// glPopMatrix();

	// glPushMatrix();
	// {
	// 	glTranslatef(132.6f + 8.200f, 18.05f, -24.26f + 49.19f);
	// 	glScalef(2.22f, 1.15f, 1.00f);
	// 	glRotatef(174.0f, 0.0f, 1.0f, 0.0f);

	// 	glTranslatef(0.9 + -64.299591, 0.1 - 4.999998 -0.3, -0.8 + -2.899999 + -51.699783);
	// 	glScalef(1.799999 * 3.999997 * 22.940430, 1.44 * 2.879998 * 14.270232 * 0.910000, 1.909999 * 14.380235);
	// 	glRotatef(25, 1, 0, 0);
	// 	glRotatef(-53, 0.0f, 1.0f, 0.0f);
	// 	fracMountain(-0.85, 0.0, -1.0, -0.85, 0.0, 0.0, 0.85, 0.0, 0.0, 0.85, 0.0, -1.0);
	// }
	// glPopMatrix();

	// Main lanka
	glPushMatrix();
	{
		glTranslatef(32.18f - 2.049, 3.049f, -30.09f + 26.64f);
		glScalef(14.0f, 4.0f, 14.0f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
		
		glPushMatrix();
		{
			glTranslatef(1.94f, -0.05f, 0.05f);
			glScalef(1.610f, 1.612f, 1.00f);
			drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
		}
		glPopMatrix();

		drawMainLanka();
	}
	glPopMatrix();

	// Main big temple
	glPushMatrix();
	{
		glTranslatef(0.810f, -0.170f, 0.310f);
	    glScalef(1.254f, 0.8200f, 1.240f);
		glScalef(1.115f, 1.10f, 1.115f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.394f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-0.285f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.735f, -0.585f, -0.165f + 4.140f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(1.739f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(3.855f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	// wall
	glBindTexture(GL_TEXTURE_2D, Texture_Base_Temple1);
	gluQuadricTexture(quadric, GL_TRUE);
	glPushMatrix();
	{
		glTranslatef(3.840f, -0.540f, -1.610f + 1.955f);
		glScalef(0.580f, 0.490f, 1.0);
		drawCubes(1.0f, 1.0f, 3.0f);
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
	{
		glTranslatef(2.745f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(1.634f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.630f, -0.555f, -2.185f);
	 	glScalef(0.455f, 0.470f, 0.455f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-0.375f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, -1.035f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.590f, -0.560f, -0.030f);
	 	glScalef(0.455f, 0.470f, 0.455f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, 0.979f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, 2.089f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple1();
	}
	glPopMatrix();

		// 	glTranslatef(objX, objY, objZ);
		// glScalef(scaleX, scaleY, scaleX);

	// Random homes

	// Set 1
	glPushMatrix();
	{
		glTranslatef(21.50f, 0.0f, -107.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(21.50f + 11.00f, 0.0f, -107.0f + -7.00f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(21.50f + 7.000f, 0.0f, -107.0f + -18.00f);

		goldenHomesSet();
	}
	glPopMatrix();

	// Set 2
	glPushMatrix();
	{
		glTranslatef(-79.50f, 0.0f, 26.50f);

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 7.000f, 0.0f, -107.0f + -18.00f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 3
	glPushMatrix();
	{
		glTranslatef(-50.50f, 0.0f, -37.00f);

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 4
	glPushMatrix();
	{
		glTranslatef(106.0f, 0.0f, 36.50f);

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 7.000f, 0.0f, -107.0f + -18.00f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 5
	glPushMatrix();
	{
		glTranslatef(112.00f, 0.0f, -41.500f);

		glPushMatrix();
		{
			glTranslatef(21.50f, 0.0f, -107.0f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 11.00f, 0.0f, -107.0f + -7.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 6
	glPushMatrix();
	{
		glTranslatef(66.50f, 0.0f, 196.00);

		glPushMatrix();
		{
			glTranslatef(21.50f, 0.0f, -107.0f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 11.00f, 0.0f, -107.0f + -7.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 7
	glPushMatrix();
	{
		glTranslatef(66.50f + 41.00f, 0.0f, 196.00 + 62.00f); 

		glPushMatrix();
		{
			glTranslatef(21.50f, 0.0f, -107.0f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 11.00f, 0.0f, -107.0f + -7.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 8
	glPushMatrix();
	{
		glTranslatef(66.50f + -66.00f, 0.0f, 196.00 + 42.50f); 

		glPushMatrix();
		{
			glTranslatef(21.50f, 0.0f, -107.0f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 11.00f, 0.0f, -107.0f + -7.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 9
	glPushMatrix();
	{
		glTranslatef(66.50f + -115.00f, 0.0f, 196.00 + -13.00f); 

		glPushMatrix();
		{
			glTranslatef(21.50f, 0.0f, -107.0f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 11.00f, 0.0f, -107.0f + -7.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 10
	glPushMatrix();
	{
		glTranslatef(66.50f + -159.00f, 0.0f, 196.00 + 19.00f); 

		glPushMatrix();
		{
			glTranslatef(21.50f, 0.0f, -107.0f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 11.00f, 0.0f, -107.0f + -7.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Set 11
	glPushMatrix();
	{
		glTranslatef(66.50f + 34.50f, 0.0f, 196.00 + -62.000f); 

		glPushMatrix();
		{
			glTranslatef(21.50f, 0.0f, -107.0f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 11.00f, 0.0f, -107.0f + -7.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 10.50f, 0.0f, -107.0f + 6.50f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + 24.50f, 0.0f, -107.0f + 2.00f);

			goldenHomesSet();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(21.50f + -6.50f, 0.0f, -107.0f + -10.50f);

			goldenHomesSet();
		}
		glPopMatrix();
	}
	glPopMatrix();

	glTranslatef(-2.05f, 0.00f, 47.14f);
	
	// 1st Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, 0.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, 0.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, 0.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, 0.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, 0.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, 0.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, 0.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, 0.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, 0.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, 0.0f);

		goldenHomesSet3();
	}
	glPopMatrix();
	
	// 2nd Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -10.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -10.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -10.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -10.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -10.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -10.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -10.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -10.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -10.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -10.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	// 3rd Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -20.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -20.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -20.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -20.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -20.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -20.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -20.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -20.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -20.0f);

		goldenHomesSet3();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -20.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	// 4th Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -30.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -30.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -30.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -30.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -30.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -30.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -30.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -30.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -30.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -30.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	// 5th Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -40.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -40.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -40.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -40.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -40.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -40.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -40.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -40.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -40.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -40.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	// 6th Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -50.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -50.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -50.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -50.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -50.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -50.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -50.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -50.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -50.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -50.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	
	// 7th Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -60.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -60.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -60.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -60.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -60.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -60.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -60.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -60.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -60.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -60.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	
	// 8th Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -70.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -70.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -70.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -70.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -70.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -70.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -70.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -70.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -70.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -70.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	
	// 9th Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -80.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -80.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -80.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -80.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -80.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -80.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -80.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -80.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -80.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -80.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	
	// 10th Row

	glPushMatrix();
	{
		glTranslatef(-9.010f, 0.0f, -90.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-18.010f, 0.0f, -90.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-27.010f, 0.0f, -90.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-36.010f, 0.0f, -90.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-45.010f, 0.0f, -90.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-54.010f, 0.0f, -90.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-63.010f, 0.0f, -90.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-72.010f, 0.0f, -90.0f);

		goldenHomesSet();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-81.010f, 0.0f, -90.0f);

		goldenHomesSet2();
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-90.010f, 0.0f, -90.0f);

		goldenHomesSet();
		glDisable(GL_LIGHTING);
	}
	glPopMatrix();
}

void goldenHomesSet()
{
	void drawTemple2(GLuint, GLuint, GLuint);
	void drawCubes(float, float, float);

	// Main big home
	glPushMatrix();
	{
		glTranslatef(0.810f, -0.170f, 0.310f);
		glScalef(1.254f, 0.8200f, 1.240f);
		glScalef(1.115f, 1.10f, 1.115f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.394f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-0.285f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.735f, -0.585f, -0.165f + 4.140f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(1.739f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(3.855f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	// wall
	glBindTexture(GL_TEXTURE_2D, Texture_Mid_Temple2);
	gluQuadricTexture(quadric, GL_TRUE);
	glPushMatrix();
	{
		glTranslatef(3.840f, -0.540f, -1.610f + 1.955f);
		glScalef(0.580f, 0.490f, 1.0);
		drawCubes(1.0f, 1.0f, 3.0f);
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
	{
		glTranslatef(2.745f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(1.634f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.630f, -0.555f, -2.185f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-0.375f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, -1.035f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.590f, -0.560f, -0.030f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, 0.979f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, 2.089f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple2, Texture_Mid_Temple2, Texture_Top_Temple2);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.70f, 9.50f, 40.50f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
		fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();
}

void goldenHomesSet3()
{
	void drawTemple2(GLuint, GLuint, GLuint);
	void drawCubes(float, float, float);

	// Main big home
	glPushMatrix();
	{
		glTranslatef(0.810f, -0.170f, 0.310f);
		glScalef(1.254f, 0.8200f, 1.240f);
		glScalef(1.115f, 1.10f, 1.115f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.394f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-0.285f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.735f, -0.585f, -0.165f + 4.140f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(1.739f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(3.855f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	// wall
	glBindTexture(GL_TEXTURE_2D, Texture_Mid_Temple4);
	gluQuadricTexture(quadric, GL_TRUE);
	glPushMatrix();
	{
		glTranslatef(3.840f, -0.540f, -1.610f + 1.955f);
		glScalef(0.580f, 0.490f, 1.0);
		drawCubes(1.0f, 1.0f, 3.0f);
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
	{
		glTranslatef(2.745f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(1.634f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.630f, -0.555f, -2.185f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-0.375f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, -1.035f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.590f, -0.560f, -0.030f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, 0.979f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, 2.089f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.70f, 9.50f, 40.50f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
			fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();
}

void drawMainLanka()
{
	void drawTemple2(GLuint, GLuint, GLuint);
	void drawCubes(float, float, float);

	// Side border 2
	glPushMatrix();
	{
		glTranslatef(-0.100f, 0.00f, -0.750f);
		// glScalef(scaleX, scaleX, 0);
		// glRotatef(rotate, 1.0f, 0.0f, 0.0f);
		glPushMatrix();
		{
			glTranslatef(-1.394f, -0.450f, 3.910f);
			glScalef(0.555f, 0.590f, 0.555f);
				drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-0.285f, -0.450f, 3.910f);
			glScalef(0.555f, 0.590f, 0.555f);
				drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.735f, -0.585f, -0.165f + 4.140f);
			glScalef(0.455f, 0.470f, 0.455f);
				drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(1.739f, -0.450f, 3.910f);
			glScalef(0.555f, 0.590f, 0.555f);
				drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(2.90f, -0.450f, 3.910f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Side border 3
	glPushMatrix();
	{
		glTranslatef(4.44f, 0.00f, -0.05f);
		glPushMatrix();
		{
			glTranslatef(-1.484f, -0.450f, -1.035f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-1.590f, -0.560f, -0.030f);
			glScalef(0.455f, 0.470f, 0.455f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-1.484f, -0.450f, 0.979f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-1.484f, -0.450f, 2.089f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();
	}
	glPopMatrix();

	// Main big home
	glPushMatrix();
	{
		glPushMatrix();
		{
			glTranslatef(0.810f + 0.050f, -0.170f + -0.250f, 0.310f + 1.300f);
			glScalef(1.115f, 1.10f, 1.115f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(2.745f, -0.450f, -2.070f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(1.634f, -0.450f, -2.070f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.630f, -0.555f, -2.185f);
			glScalef(0.455f, 0.470f, 0.455f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-0.375f, -0.450f, -2.070f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-1.484f, -0.450f, -2.070f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-1.484f, -0.450f, -1.035f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-1.590f, -0.560f, -0.030f);
			glScalef(0.455f, 0.470f, 0.455f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-1.484f, -0.450f, 0.979f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(-1.484f, -0.450f, 2.089f);
			glScalef(0.555f, 0.590f, 0.555f);
			drawTemple2(Texture_Base_Temple4, Texture_Mid_Temple4, Texture_Mid_Temple4);
		}
		glPopMatrix();
	}
	glPopMatrix();
}

void goldenHomesSet2()
{
	void drawTemple2(GLuint, GLuint, GLuint);
	void drawCubes(float, float, float);

	// Main big home
	glPushMatrix();
	{
		glTranslatef(0.810f, -0.170f, 0.310f);
		glScalef(1.254f, 0.8200f, 1.240f);
		glScalef(1.115f, 1.10f, 1.115f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.394f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-0.285f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.735f, -0.585f, -0.165f + 4.140f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(1.739f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(3.855f, -0.450f, 3.910f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	// wall
	glBindTexture(GL_TEXTURE_2D, Texture_Mid_Temple3);
	gluQuadricTexture(quadric, GL_TRUE);
	glPushMatrix();
	{
		glTranslatef(3.840f, -0.540f, -1.610f + 1.955f);
		glScalef(0.580f, 0.490f, 1.0);
		drawCubes(1.0f, 1.0f, 3.0f);
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
	{
		glTranslatef(2.745f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(1.634f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.630f, -0.555f, -2.185f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-0.375f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, -2.070f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, -1.035f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.590f, -0.560f, -0.030f);
		glScalef(0.455f, 0.470f, 0.455f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, 0.979f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(-1.484f, -0.450f, 2.089f);
		glScalef(0.555f, 0.590f, 0.555f);
		drawTemple2(Texture_Base_Temple3, Texture_Mid_Temple3, Texture_Top_Temple3);
	}
	glPopMatrix();

	glPushMatrix();
	{
		glTranslatef(0.70f, 9.50f, 40.50f);
		if(vec.size() < (palaceCounter + 1))
		{
			Fire fire;
			vec.insert(vec.begin() + palaceCounter, fire);
		}
		if((Global_Time - LankaDahan + 1) * Global_Time / 6 >= palaceCounter)
		{
			Fire *fire = &vec[palaceCounter];
			fire->display();
		}

		palaceCounter++;
	}
	glPopMatrix();
}

void drawTemple1()
{
	void drawPyramid();
	void drawCubes(float width, float height, float depth);

	// temple1 base 
	glBindTexture(GL_TEXTURE_2D, Texture_Base_Temple1);
	gluQuadricTexture(quadric, GL_TRUE);
	glPushMatrix();
	{
		drawCubes(1.0f, 1.0f, 1.0f);
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// temple1 top
	glBindTexture(GL_TEXTURE_2D, Texture_Top_Temple1);
	gluQuadricTexture(quadric, GL_TRUE);
	glPushMatrix();
	{
		glTranslatef(0.00f, 2.009f, -6.0f + 5.97f);
		glScalef(0.99f, 1.01f, 0.99f);

		drawPyramid();
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void drawTemple2(GLuint baseTexture, GLuint midTexture, GLuint topTexture)
{
	void drawPyramid();
	void drawCubes(float width, float height, float depth);

	// temple2 base 
	glBindTexture(GL_TEXTURE_2D, baseTexture);
	gluQuadricTexture(quadric, GL_TRUE);
	glPushMatrix();
	{
		// glScalef(-0.010f, 0.0f, 0.0f);
		drawCubes(1.0f, 1.0f, 1.0f);
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glPushMatrix();
	{
		glTranslatef(0.020f, 0.020f, -0.010f);
		glScalef(1.08f, 1.00f, 1.08f);

		// temple2 mid 
		glBindTexture(GL_TEXTURE_2D, midTexture);
		gluQuadricTexture(quadric, GL_TRUE);
		glPushMatrix();
		{
			glTranslatef(-0.020f, 1.839f, 0.000f);
			glScalef(0.875f, 0.870f, 0.875f);
			drawCubes(1.0f, 1.0f, 1.0f);
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		// temple2 top
		glBindTexture(GL_TEXTURE_2D, topTexture);
		gluQuadricTexture(quadric, GL_TRUE);
		glPushMatrix();
		{
			glTranslatef(-0.020f, 4.430f, 0.010f);
			glScalef(0.874f, 1.719f, 0.874f);
			glScalef(0.99f, 1.01f, 0.99f);
			drawPyramid();
		}
		glPopMatrix();
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
}

void drawPyramid()
{
		// Pyramid 
		glBegin(GL_TRIANGLES);

		// Front face
		glTexCoord2f(0.5, 1.0);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);

		// Right face
		glTexCoord2f(0.5, 1.0);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 1.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);

		// Back face
		glTexCoord2f(0.5f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, -1.0f);

		// Left face
		glTexCoord2f(0.5f, 1.0f);
		glVertex3f(0.0f, 1.0f, 0.0f);
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, -1.0f);
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 1.0f);

		glEnd();
}

void drawCubes(float width, float height, float depth)
{
	glBegin(GL_QUADS);

	// Front face
	// Step 14 : Map image to the texture co-ordinates
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(width,  height, depth);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-width,  height, depth);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-width, - height, depth);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(width, - height, depth);

	// Right face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(width, height, -depth);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(width,  height, depth);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(width, - height, depth);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(width, -height, -depth);

	// Back face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-width, height, -depth);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(width, height, -depth);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(width, -height, -depth);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-width, -height, -depth);

	// Left face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-width,  height, depth);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-width, height, -depth);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-width, -height, -depth);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-width, - height, depth);

	// Top face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(width, height, -depth);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-width, height, -depth);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-width,  height, depth);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(width,  height, depth);

	// Bottom face
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(width, -height, -depth);
	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-width, -height, -depth);
	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-width, - height, depth);
	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(width, - height, depth);

	glEnd();
}

void drawBalHanumanEatingSun()
{
	if(hanumanEatingScenePart == 1)
	{
		lightAmbient[0] = hanumanEatingSceneLight;
		lightAmbient[1] = hanumanEatingSceneLight;
		lightAmbient[2] = hanumanEatingSceneLight;
		lightAmbient[3] = hanumanEatingSceneLight;

		lightDiffuse[0] = hanumanEatingSceneLight;
		lightDiffuse[1] = hanumanEatingSceneLight;
		lightDiffuse[2] = hanumanEatingSceneLight;
		lightDiffuse[3] = hanumanEatingSceneLight;

		materialAmbient[0] = hanumanEatingSceneLight;
		materialAmbient[1] = hanumanEatingSceneLight;
		materialAmbient[2] = hanumanEatingSceneLight;
		materialAmbient[3] = hanumanEatingSceneLight;

		materialDiffuse[0] = hanumanEatingSceneLight;
		materialDiffuse[1] = hanumanEatingSceneLight;
		materialDiffuse[2] = hanumanEatingSceneLight;
		materialDiffuse[3] = hanumanEatingSceneLight;
		
		// light related initialization
		glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

		// material related initialization
		glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
		// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

		//glDisable(GL_LIGHTING);
		///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			//glEnable(GL_LIGHTING);
			glTranslatef(0.0f, 0.0f, 0.0f);     
			glRotatef(galaxyMovingY, 0.0f, 0.0f, 1.0f);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		// Flying hanuman
		glEnable(GL_BLEND);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindTexture(GL_TEXTURE_2D, Texture_FlyingHanuman);

		glPushMatrix();
		{
			glTranslatef(0.70f + -0.1 + babyHanumanFlyingX, 0.250f, -3.00f + -1.85 + babyHanumanFlyingZ);
			// glScalef(scaleX, scaleX, scaleX);
			glRotatef(-9.00f, 0.0, 0, 1);
			glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);
			// Right top
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			// Left top
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			// Left bottom
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			// Right bottom
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glEnd();
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		glDisable(GL_BLEND);

		///////////////////////////////////////////// Sun /////////////////////////////////////////////
		glPushMatrix();
		{
			glBindTexture(GL_TEXTURE_2D, Texture_Sun);
			gluQuadricTexture(quadric, GL_TRUE);
			
			glPushMatrix();
			{
				//glEnable(GL_LIGHTING);
				glTranslatef(-0.30f + translateSunX, 0.00f, -1.505f + translateSunZ);
				glScalef(0.20f, 0.20f, 0.20f);
				// glTranslatef(objX, objY, 0.0f);
				gluSphere(quadric, 1.0f, 50, 50); // gluSphere() create all needed normals

				// Sun toppings
				glEnable(GL_BLEND);

				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				glBindTexture(GL_TEXTURE_2D, Texture_SunToppings);

				glPushMatrix();
				{
					glTranslatef(-0.05, 0.00, 0.00);
					glScalef(1.35, 1.35, 1.35);
					glRotatef(rotateSunToppings, 0, 0, 1);
					glBegin(GL_QUADS);
					glColor3f(1.0f, 1.0f, 1.0f);
					// Right top
					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, 1.0f, 0.0f);

					// Left top
					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, 1.0f, 0.0f);

					// Left bottom
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, -1.0f, 0.0f);

					// Right bottom
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, -1.0f, 0.0f);

					glEnd();
				}
				glPopMatrix();

				glPushMatrix();
				{
					glTranslatef(-0.05, 0.00, 0.00);
					glScalef(1.35, 1.35, 1.35);
					glRotatef(-rotateSunToppings, 0, 0, 1);
					glBegin(GL_QUADS);
					glColor3f(1.0f, 1.0f, 1.0f);
					// Right top
					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, 1.0f, 0.0f);

					// Left top
					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, 1.0f, 0.0f);

					// Left bottom
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, -1.0f, 0.0f);

					// Right bottom
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, -1.0f, 0.0f);

					glEnd();
				}
				glPopMatrix();

				glPushMatrix();
				{
					glTranslatef(-0.05, 0.00, 0.00);
					glScalef(1.35, 1.35, 1.35);
					glRotatef(rotateSunToppings, 0, 0, 1);
					glBegin(GL_QUADS);
					glColor3f(1.0f, 1.0f, 1.0f);
					// Right top
					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, 1.0f, 0.0f);

					// Left top
					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, 1.0f, 0.0f);

					// Left bottom
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, -1.0f, 0.0f);

					// Right bottom
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, -1.0f, 0.0f);

					glEnd();
				}
				glPopMatrix();
				glBindTexture(GL_TEXTURE_2D, 0);

				glDisable(GL_BLEND);
			}	
			glPopMatrix();
			glBindTexture(GL_TEXTURE_2D, 0);
		}	
		glPopMatrix();
	}
	else if(hanumanEatingScenePart == 2)
	{
		// if(hanumanEatingScenePart == 2) {
			// gluLookAt(
			// 	0.0f, hanumanFallingCameraY, hanumanFallingCameraZ, // eye position
			// 	0.0f, 0.0, 0.0f, // eye center
			// 	0.0f, 1.0f, 0.0f // up vector
			// );
		// }
		// gluLookAt(
		// 	eyeX, eyeY, eyeZ, // eye position
		// 	0.0f, 0.0, -10.0f, // eye center
		// 	0.0f, 1.0f, 0.0f // up vector
		// );

		// glTranslatef(0.0f, 0.0f, -50.0f);

		lightAmbient[0] = hanumanEatingSceneLight;
		lightAmbient[1] = hanumanEatingSceneLight;
		lightAmbient[2] = hanumanEatingSceneLight;
		lightAmbient[3] = hanumanEatingSceneLight;

		lightDiffuse[0] = hanumanEatingSceneLight;
		lightDiffuse[1] = hanumanEatingSceneLight;
		lightDiffuse[2] = hanumanEatingSceneLight;
		lightDiffuse[3] = hanumanEatingSceneLight;

		materialAmbient[0] = hanumanEatingSceneLight;
		materialAmbient[1] = hanumanEatingSceneLight;
		materialAmbient[2] = hanumanEatingSceneLight;
		materialAmbient[3] = hanumanEatingSceneLight;

		materialDiffuse[0] = hanumanEatingSceneLight;
		materialDiffuse[1] = hanumanEatingSceneLight;
		materialDiffuse[2] = hanumanEatingSceneLight;
		materialDiffuse[3] = hanumanEatingSceneLight;
		
		// light related initialization
		glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

		// material related initialization
		glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
		// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

		///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			glEnable(GL_LIGHTING);
			glTranslatef(objX, objY, objZ);     
			glRotatef(galaxyMovingY, 0.0f, 0.0f, 1.0f);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		glPushMatrix();
		{
			// Step 2: Rotate around the x-axis to perform the rotation

			// glRotatef(rotate, 1.0f, 0.0f, 0.0f);
			// glTranslatef(1.0f, 0.0f, 1.0f);
			///////////////////////////////////////////// Left hand /////////////////////////////////////////////
			// set polygon mode to filled (although filled polygon is default already hence not needed)
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glPushMatrix();
			{
				// do initial translation for better visibility
				glTranslatef(-6.074 + -0.645f, -3.885 + -1.184f, -12.0f);	
				
				glRotatef(302.0f, 1.0f, 0.0f, 0.0f);
				glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
				//glRotatef(rotate, 0.0f, 0.0f, 1.0f);
				// push this matrix
				glPushMatrix();
				{
					// do transformation for arm
					// z-axis is in the direction of x-axis because of problem in glu library
					glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
					glRotatef(-handsMovement, 0.0f, 1.0f, 0.0f);

					glTranslatef(1.0f, 0.0f, 0.0f);
					glScalef(1.5f, 1.0f, 1.0f);

					glBindTexture(GL_TEXTURE_2D, Texture_BalHanumanBiceps);
					gluQuadricTexture(quadric, GL_TRUE);

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);
						glRotatef(338.0f, 1.0f, 0.0f, 0.0f);

						// draw arm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.2f, 10, 10);

						// pop the matrix to come back where arm ended
					}
					glPopMatrix();

					// do transformation for forarm in current transformation matrix (mtrt or f)
					glTranslatef(1.0f, 0.0f, 0.0f);
					glRotatef(344.0f, 0.0f, 0.0f, 1.0f);
					glTranslatef(1.0f + 0.839f, 0.0f + -0.165f, 0.0f + 0.465f);	

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);

						// Draw the forarm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.1f, 10, 10);

					}
					glPopMatrix();

					// do transformation for hand in current transformation matrix (mtrt or f)
					glTranslatef(1.0f + -0.045f, 0.0f + 0.030f, 0.0f + 0.0f);
					glRotatef(344.0f, 0.0f, 0.0f, 1.0f);
					glTranslatef(0.35f, 0.0f, 0.0f);

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);

						// Draw the forarm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.0f, 10, 10);
					}
					glPopMatrix();
					glBindTexture(GL_TEXTURE_2D, 0);
				}
				glPopMatrix();
			}
			glPopMatrix();

			///////////////////////////////////////////// Right hand /////////////////////////////////////////////

			glPushMatrix();
			{
				// do initial translation for better visibility
				glTranslatef(-6.074 + -0.645f + 14.010 + -0.794, -3.885 + -1.184f + -0.135 + 0.300, -12.0f);	
				
				glRotatef(302.0f, 1.0f, 0.0f, 0.0f);
				glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
				
				//glRotatef(rotate, 0.0f, 0.0f, 1.0f);
				// push this matrix
				glPushMatrix();
				{

					// do transformation for arm
					// z-axis is in the direction of x-axis because of problem in glu library
					glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
					glRotatef(-handsMovement, 0.0f, 1.0f, 0.0f);
					glTranslatef(1.0f, 0.0f, 0.0f);
					glScalef(1.5f, 1.0f, 1.0f);

					glBindTexture(GL_TEXTURE_2D, Texture_BalHanumanBiceps);
					gluQuadricTexture(quadric, GL_TRUE);

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);
						glRotatef(338.0f, 1.0f, 0.0f, 0.0f);
						glRotatef(1361.0f, 1.0, 0.0f, 0.0f);

						// draw arm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.2f, 10, 10);

						// pop the matrix to come back where arm ended
					}
					glPopMatrix();
			
					// do transformation for forarm in current transformation matrix (mtrt or f)
					glTranslatef(1.0f, 0.0f, 0.0f);
					glRotatef(344.0f + 401.0f, 0.0f, 0.0f, 1.0f);
					glTranslatef(1.0f + 0.839f + -0.120f, 0.0f + -0.165f + 0.165f, 0.0f + 0.465f);	

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);
						glRotatef(245.0f, 1.0, 0.0f, 0.0f);

						// Draw the forarm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.1f, 10, 10);

					}
					glPopMatrix();

					glBindTexture(GL_TEXTURE_2D, 0);
				}
				glPopMatrix();
			}
			glPopMatrix();
			glDisable(GL_LIGHTING);
		}
		glPopMatrix();

		///////////////////////////////////////////// Sun /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Sun);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			glEnable(GL_LIGHTING);
			glTranslatef(0.0f, 0.0 - sunTranslateY, -3.0f + sunTranslateZ);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 1.0f, 50, 50); // gluSphere() create all needed normals

			// Sun toppings
			glEnable(GL_BLEND);

			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			glBindTexture(GL_TEXTURE_2D, Texture_SunToppings);

			glPushMatrix();
			{
				glTranslatef(-0.05, 0.00, 0.00);
				glScalef(1.35, 1.35, 1.35);
				glRotatef(rotateSunToppings, 0, 0, 1);
				glBegin(GL_QUADS);
				glColor3f(1.0f, 1.0f, 1.0f);
				// Right top
				glTexCoord2f(1.0f, 1.0f);
				glVertex3f(1.0f, 1.0f, 0.0f);

				// Left top
				glTexCoord2f(0.0f, 1.0f);
				glVertex3f(-1.0f, 1.0f, 0.0f);

				// Left bottom
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(-1.0f, -1.0f, 0.0f);

				// Right bottom
				glTexCoord2f(1.0f, 0.0f);
				glVertex3f(1.0f, -1.0f, 0.0f);

				glEnd();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.05, 0.00, 0.00);
				glScalef(1.35, 1.35, 1.35);
				glRotatef(-rotateSunToppings, 0, 0, 1);
				glBegin(GL_QUADS);
				glColor3f(1.0f, 1.0f, 1.0f);
				// Right top
				glTexCoord2f(1.0f, 1.0f);
				glVertex3f(1.0f, 1.0f, 0.0f);

				// Left top
				glTexCoord2f(0.0f, 1.0f);
				glVertex3f(-1.0f, 1.0f, 0.0f);

				// Left bottom
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(-1.0f, -1.0f, 0.0f);

				// Right bottom
				glTexCoord2f(1.0f, 0.0f);
				glVertex3f(1.0f, -1.0f, 0.0f);

				glEnd();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.05, 0.00, 0.00);
				glScalef(1.35, 1.35, 1.35);
				glRotatef(rotateSunToppings, 0, 0, 1);
				glBegin(GL_QUADS);
				glColor3f(1.0f, 1.0f, 1.0f);
				// Right top
				glTexCoord2f(1.0f, 1.0f);
				glVertex3f(1.0f, 1.0f, 0.0f);

				// Left top
				glTexCoord2f(0.0f, 1.0f);
				glVertex3f(-1.0f, 1.0f, 0.0f);

				// Left bottom
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(-1.0f, -1.0f, 0.0f);

				// Right bottom
				glTexCoord2f(1.0f, 0.0f);
				glVertex3f(1.0f, -1.0f, 0.0f);

				glEnd();
			}
			glPopMatrix();
			glBindTexture(GL_TEXTURE_2D, 0);

			glDisable(GL_BLEND);
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);


	}
	else
	{
		lightAmbient[0] = earthSceneLight;
		lightAmbient[1] = earthSceneLight;
		lightAmbient[2] = earthSceneLight;
		lightAmbient[3] = earthSceneLight;

		lightDiffuse[0] = earthSceneLight;
		lightDiffuse[1] = earthSceneLight;
		lightDiffuse[2] = earthSceneLight;
		lightDiffuse[3] = earthSceneLight;

		materialAmbient[0] = earthSceneLight;
		materialAmbient[1] = earthSceneLight;
		materialAmbient[2] = earthSceneLight;
		materialAmbient[3] = earthSceneLight;

		materialDiffuse[0] = earthSceneLight;
		materialDiffuse[1] = earthSceneLight;
		materialDiffuse[2] = earthSceneLight;
		materialDiffuse[3] = earthSceneLight;
		
		// light related initialization
		glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

		// material related initialization
		glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
		// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

		///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			//glEnable(GL_LIGHTING);
			glTranslatef(0.0f, 0.0f, 0.0f);     
			glRotatef(galaxyMovingY, 0.0f, 0.0f, 1.0f);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		///////////////////////////////////////////// Earth /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Earth);
		gluQuadricTexture(quadric, GL_TRUE);

		glPushMatrix();
		{
			glEnable(GL_LIGHTING);
			glTranslatef(1.30f, -1.50f, -9.25f);
			glRotatef(246.0f, 1, 1, 1);
			gluSphere(quadric, 6.0f, 50, 50); // gluSphere() create all needed normals fo
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);	
	}
}

/*
void drawBalHanumanEatingSun()
{
	hanumanEatingScenePart = 2;
	if(hanumanEatingScenePart == 1)
	{
		lightAmbient[0] = hanumanEatingSceneLight;
		lightAmbient[1] = hanumanEatingSceneLight;
		lightAmbient[2] = hanumanEatingSceneLight;
		lightAmbient[3] = hanumanEatingSceneLight;

		lightDiffuse[0] = hanumanEatingSceneLight;
		lightDiffuse[1] = hanumanEatingSceneLight;
		lightDiffuse[2] = hanumanEatingSceneLight;
		lightDiffuse[3] = hanumanEatingSceneLight;

		materialAmbient[0] = hanumanEatingSceneLight;
		materialAmbient[1] = hanumanEatingSceneLight;
		materialAmbient[2] = hanumanEatingSceneLight;
		materialAmbient[3] = hanumanEatingSceneLight;

		materialDiffuse[0] = hanumanEatingSceneLight;
		materialDiffuse[1] = hanumanEatingSceneLight;
		materialDiffuse[2] = hanumanEatingSceneLight;
		materialDiffuse[3] = hanumanEatingSceneLight;
		
		// light related initialization
		glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

		// material related initialization
		glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
		// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

		//glDisable(GL_LIGHTING);
		///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			//glEnable(GL_LIGHTING);
			glTranslatef(0.0f, 0.0f, 0.0f);     
			glRotatef(galaxyMovingY, 0.0f, 0.0f, 1.0f);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		// Flying hanuman
		glEnable(GL_BLEND);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindTexture(GL_TEXTURE_2D, Texture_FlyingHanuman);

		glPushMatrix();
		{
			glTranslatef(0.70f + -0.1 + babyHanumanFlyingX, 0.250f, -3.00f + -1.85 + babyHanumanFlyingZ);
			// glScalef(scaleX, scaleX, scaleX);
			glRotatef(-9.00f, 0.0, 0, 1);
			glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);
			// Right top
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			// Left top
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			// Left bottom
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			// Right bottom
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glEnd();
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		glDisable(GL_BLEND);

		///////////////////////////////////////////// Sun /////////////////////////////////////////////
		glPushMatrix();
		{
			glBindTexture(GL_TEXTURE_2D, Texture_Sun);
			gluQuadricTexture(quadric, GL_TRUE);
			
			glPushMatrix();
			{
				//glEnable(GL_LIGHTING);
				glTranslatef(-0.30f + translateSunX, 0.00f, -1.50f + translateSunZ);
				glScalef(0.20f, 0.20f, 0.20f);
				// glTranslatef(objX, objY, 0.0f);
				gluSphere(quadric, 1.0f, 50, 50); // gluSphere() create all needed normals

				// Sun toppings
				glEnable(GL_BLEND);

				glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

				glBindTexture(GL_TEXTURE_2D, Texture_SunToppings);

				glPushMatrix();
				{
					glTranslatef(-0.05, 0.00, 0.00);
					glScalef(1.35, 1.35, 1.35);
					glRotatef(rotateSunToppings, 0, 0, 1);
					glBegin(GL_QUADS);
					glColor3f(1.0f, 1.0f, 1.0f);
					// Right top
					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, 1.0f, 0.0f);

					// Left top
					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, 1.0f, 0.0f);

					// Left bottom
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, -1.0f, 0.0f);

					// Right bottom
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, -1.0f, 0.0f);

					glEnd();
				}
				glPopMatrix();

				glPushMatrix();
				{
					glTranslatef(-0.05, 0.00, 0.00);
					glScalef(1.35, 1.35, 1.35);
					glRotatef(-rotateSunToppings, 0, 0, 1);
					glBegin(GL_QUADS);
					glColor3f(1.0f, 1.0f, 1.0f);
					// Right top
					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, 1.0f, 0.0f);

					// Left top
					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, 1.0f, 0.0f);

					// Left bottom
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, -1.0f, 0.0f);

					// Right bottom
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, -1.0f, 0.0f);

					glEnd();
				}
				glPopMatrix();

				glPushMatrix();
				{
					glTranslatef(-0.05, 0.00, 0.00);
					glScalef(1.35, 1.35, 1.35);
					glRotatef(rotateSunToppings, 0, 0, 1);
					glBegin(GL_QUADS);
					glColor3f(1.0f, 1.0f, 1.0f);
					// Right top
					glTexCoord2f(1.0f, 1.0f);
					glVertex3f(1.0f, 1.0f, 0.0f);

					// Left top
					glTexCoord2f(0.0f, 1.0f);
					glVertex3f(-1.0f, 1.0f, 0.0f);

					// Left bottom
					glTexCoord2f(0.0f, 0.0f);
					glVertex3f(-1.0f, -1.0f, 0.0f);

					// Right bottom
					glTexCoord2f(1.0f, 0.0f);
					glVertex3f(1.0f, -1.0f, 0.0f);

					glEnd();
				}
				glPopMatrix();
				glBindTexture(GL_TEXTURE_2D, 0);

				glDisable(GL_BLEND);
			}	
			glPopMatrix();
			glBindTexture(GL_TEXTURE_2D, 0);
		}	
		glPopMatrix();
	}
	else if(hanumanEatingScenePart == 2)
	{
		// if(hanumanEatingScenePart == 2) {
		// gluLookAt(
		// 	0.0f, hanumanFallingCameraY, hanumanFallingCameraZ, // eye position
		// 	0.0f, 0.0, 0.0f, // eye center
		// 	0.0f, 1.0f, 0.0f // up vector
		// );
		// }
		// gluLookAt(
		// 	eyeX, eyeY, eyeZ, // eye position
		// 	0.0f, 0.0, -10.0f, // eye center
		// 	0.0f, 1.0f, 0.0f // up vector
		// );

		// glTranslatef(0.0f, 0.0f, -50.0f);

		lightAmbient[0] = hanumanEatingSceneLight;
		lightAmbient[1] = hanumanEatingSceneLight;
		lightAmbient[2] = hanumanEatingSceneLight;
		lightAmbient[3] = hanumanEatingSceneLight;

		lightDiffuse[0] = hanumanEatingSceneLight;
		lightDiffuse[1] = hanumanEatingSceneLight;
		lightDiffuse[2] = hanumanEatingSceneLight;
		lightDiffuse[3] = hanumanEatingSceneLight;

		materialAmbient[0] = hanumanEatingSceneLight;
		materialAmbient[1] = hanumanEatingSceneLight;
		materialAmbient[2] = hanumanEatingSceneLight;
		materialAmbient[3] = hanumanEatingSceneLight;

		materialDiffuse[0] = hanumanEatingSceneLight;
		materialDiffuse[1] = hanumanEatingSceneLight;
		materialDiffuse[2] = hanumanEatingSceneLight;
		materialDiffuse[3] = hanumanEatingSceneLight;
		
		// light related initialization
		glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

		// material related initialization
		glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
		// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

		///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			glEnable(GL_LIGHTING);
			glTranslatef(objX, objY, objZ);     
			glRotatef(galaxyMovingY, 0.0f, 0.0f, 1.0f);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		///////////////////////////////////////////// Sun /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Sun);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			glEnable(GL_LIGHTING);
			glTranslatef(0.0f, 0.0f - sunTranslateY, -3.0f + sunTranslateZ);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 1.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		if(hanumanEatingScenePart == 1)
		{
			glPushMatrix();
			{
				// Step 2: Rotate around the x-axis to perform the rotation

				// glRotatef(rotate, 1.0f, 0.0f, 0.0f);
				// glTranslatef(1.0f, 0.0f, 1.0f);
				///////////////////////////////////////////// Left hand /////////////////////////////////////////////
				// set polygon mode to filled (although filled polygon is default already hence not needed)
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				glPushMatrix();
				{
					// do initial translation for better visibility
					glTranslatef(-6.074 + -0.645f, -3.885 + -1.184f, -12.0f);	
					
					glRotatef(302.0f, 1.0f, 0.0f, 0.0f);
					glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
					//glRotatef(rotate, 0.0f, 0.0f, 1.0f);
					// push this matrix
					glPushMatrix();
					{
						// do transformation for arm
						// z-axis is in the direction of x-axis because of problem in glu library
						glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
						glRotatef(-handsMovement, 0.0f, 1.0f, 0.0f);

						glTranslatef(1.0f, 0.0f, 0.0f);
						glScalef(1.5f, 1.0f, 1.0f);

						glBindTexture(GL_TEXTURE_2D, Texture_BalHanumanBiceps);
						gluQuadricTexture(quadric, GL_TRUE);

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);
							glRotatef(338.0f, 1.0f, 0.0f, 0.0f);

							// draw arm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.2f, 10, 10);

							// pop the matrix to come back where arm ended
						}
						glPopMatrix();

						// do transformation for forarm in current transformation matrix (mtrt or f)
						glTranslatef(1.0f, 0.0f, 0.0f);
						glRotatef(344.0f, 0.0f, 0.0f, 1.0f);
						glTranslatef(1.0f + 0.839f, 0.0f + -0.165f, 0.0f + 0.465f);	

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);

							// Draw the forarm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.1f, 10, 10);

						}
						glPopMatrix();

						// do transformation for hand in current transformation matrix (mtrt or f)
						glTranslatef(1.0f + -0.045f, 0.0f + 0.030f, 0.0f + 0.0f);
						glRotatef(344.0f, 0.0f, 0.0f, 1.0f);
						glTranslatef(0.35f, 0.0f, 0.0f);

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);

							// Draw the forarm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.0f, 10, 10);
						}
						glPopMatrix();
						glBindTexture(GL_TEXTURE_2D, 0);
					}
					glPopMatrix();
				}
				glPopMatrix();

				///////////////////////////////////////////// Right hand /////////////////////////////////////////////

				glPushMatrix();
				{
					// do initial translation for better visibility
					glTranslatef(-6.074 + -0.645f + 14.010 + -0.794, -3.885 + -1.184f + -0.135 + 0.300, -12.0f);	
					
					glRotatef(302.0f, 1.0f, 0.0f, 0.0f);
					glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
					
					//glRotatef(rotate, 0.0f, 0.0f, 1.0f);
					// push this matrix
					glPushMatrix();
					{

						// do transformation for arm
						// z-axis is in the direction of x-axis because of problem in glu library
						glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
						glRotatef(-handsMovement, 0.0f, 1.0f, 0.0f);
						glTranslatef(1.0f, 0.0f, 0.0f);
						glScalef(1.5f, 1.0f, 1.0f);

						glBindTexture(GL_TEXTURE_2D, Texture_BalHanumanBiceps);
						gluQuadricTexture(quadric, GL_TRUE);

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);
							glRotatef(338.0f, 1.0f, 0.0f, 0.0f);
							glRotatef(1361.0f, 1.0, 0.0f, 0.0f);

							// draw arm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.2f, 10, 10);

							// pop the matrix to come back where arm ended
						}
						glPopMatrix();
				
						// do transformation for forarm in current transformation matrix (mtrt or f)
						glTranslatef(1.0f, 0.0f, 0.0f);
						glRotatef(344.0f + 401.0f, 0.0f, 0.0f, 1.0f);
						glTranslatef(1.0f + 0.839f + -0.120f, 0.0f + -0.165f + 0.165f, 0.0f + 0.465f);	

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);
							glRotatef(245.0f, 1.0, 0.0f, 0.0f);

							// Draw the forarm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.1f, 10, 10);

						}
						glPopMatrix();

						glBindTexture(GL_TEXTURE_2D, 0);
					}
					glPopMatrix();
				}
				glPopMatrix();
				glDisable(GL_LIGHTING);
			}
			glPopMatrix();
		}
	}
	/* else if(hanumanEatingScenePart == 2)
	{
		lightAmbient[0] = hanumanEatingSceneLight;
		lightAmbient[1] = hanumanEatingSceneLight;
		lightAmbient[2] = hanumanEatingSceneLight;
		lightAmbient[3] = hanumanEatingSceneLight;

		lightDiffuse[0] = hanumanEatingSceneLight;
		lightDiffuse[1] = hanumanEatingSceneLight;
		lightDiffuse[2] = hanumanEatingSceneLight;
		lightDiffuse[3] = hanumanEatingSceneLight;

		materialAmbient[0] = hanumanEatingSceneLight;
		materialAmbient[1] = hanumanEatingSceneLight;
		materialAmbient[2] = hanumanEatingSceneLight;
		materialAmbient[3] = hanumanEatingSceneLight;

		materialDiffuse[0] = hanumanEatingSceneLight;
		materialDiffuse[1] = hanumanEatingSceneLight;
		materialDiffuse[2] = hanumanEatingSceneLight;
		materialDiffuse[3] = hanumanEatingSceneLight;
		
		// light related initialization
		glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

		// material related initialization
		glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
		// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

		//glDisable(GL_LIGHTING);
		///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			//glEnable(GL_LIGHTING);
			glTranslatef(0.0f, 0.0f, 0.0f);     
			glRotatef(galaxyMovingY, 0.0f, 0.0f, 1.0f);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		// Flying hanuman
		glEnable(GL_BLEND);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glBindTexture(GL_TEXTURE_2D, Texture_FlyingHanuman);

		glPushMatrix();
		{
			glTranslatef(0.70f + -0.1 + babyHanumanFlyingX, 0.250f, -3.00f + -1.85 + babyHanumanFlyingZ);
			// glScalef(scaleX, scaleX, scaleX);
			glRotatef(-9.00f, 0.0, 0, 1);
			glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);
			// Right top
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			// Left top
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			// Left bottom
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			// Right bottom
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glEnd();
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		glDisable(GL_BLEND);

		///////////////////////////////////////////// Sun /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Sun);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			//glEnable(GL_LIGHTING);
			glTranslatef(-0.30f + translateSunX, 0.00f, -1.50f + translateSunZ);
			glScalef(0.20f, 0.20f, 0.20f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 1.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		glPushMatrix();
		{
			// Step 2: Rotate around the x-axis to perform the rotation

			// glRotatef(rotate, 1.0f, 0.0f, 0.0f);
			// glTranslatef(1.0f, 0.0f, 1.0f);
			///////////////////////////////////////////// Left hand /////////////////////////////////////////////
			// set polygon mode to filled (although filled polygon is default already hence not needed)
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
			glPushMatrix();
			{
				// do initial translation for better visibility
				glTranslatef(-6.074 + -0.645f, -3.885 + -1.184f, -12.0f);	
				
				glRotatef(302.0f, 1.0f, 0.0f, 0.0f);
				glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
				//glRotatef(rotate, 0.0f, 0.0f, 1.0f);
				// push this matrix
				glPushMatrix();
				{
					// do transformation for arm
					// z-axis is in the direction of x-axis because of problem in glu library
					glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
					glRotatef(-handsMovement, 0.0f, 1.0f, 0.0f);

					glTranslatef(1.0f, 0.0f, 0.0f);
					glScalef(1.5f, 1.0f, 1.0f);

					glBindTexture(GL_TEXTURE_2D, Texture_BalHanumanBiceps);
					gluQuadricTexture(quadric, GL_TRUE);

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);
						glRotatef(338.0f, 1.0f, 0.0f, 0.0f);

						// draw arm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.2f, 10, 10);

						// pop the matrix to come back where arm ended
					}
					glPopMatrix();

					// do transformation for forarm in current transformation matrix (mtrt or f)
					glTranslatef(1.0f, 0.0f, 0.0f);
					glRotatef(344.0f, 0.0f, 0.0f, 1.0f);
					glTranslatef(1.0f + 0.839f, 0.0f + -0.165f, 0.0f + 0.465f);	

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);

						// Draw the forarm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.1f, 10, 10);

					}
					glPopMatrix();

					// do transformation for hand in current transformation matrix (mtrt or f)
					glTranslatef(1.0f + -0.045f, 0.0f + 0.030f, 0.0f + 0.0f);
					glRotatef(344.0f, 0.0f, 0.0f, 1.0f);
					glTranslatef(0.35f, 0.0f, 0.0f);

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);

						// Draw the forarm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.0f, 10, 10);
					}
					glPopMatrix();
					glBindTexture(GL_TEXTURE_2D, 0);
				}
				glPopMatrix();
			}
			glPopMatrix();

			///////////////////////////////////////////// Right hand /////////////////////////////////////////////

			glPushMatrix();
			{
				// do initial translation for better visibility
				glTranslatef(-6.074 + -0.645f + 14.010 + -0.794, -3.885 + -1.184f + -0.135 + 0.300, -12.0f);	
				
				glRotatef(302.0f, 1.0f, 0.0f, 0.0f);
				glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
				
				//glRotatef(rotate, 0.0f, 0.0f, 1.0f);
				// push this matrix
				glPushMatrix();
				{

					// do transformation for arm
					// z-axis is in the direction of x-axis because of problem in glu library
					glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
					glRotatef(-handsMovement, 0.0f, 1.0f, 0.0f);
					glTranslatef(1.0f, 0.0f, 0.0f);
					glScalef(1.5f, 1.0f, 1.0f);

					glBindTexture(GL_TEXTURE_2D, Texture_BalHanumanBiceps);
					gluQuadricTexture(quadric, GL_TRUE);

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);
						glRotatef(338.0f, 1.0f, 0.0f, 0.0f);
						glRotatef(1361.0f, 1.0, 0.0f, 0.0f);

						// draw arm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.2f, 10, 10);

						// pop the matrix to come back where arm ended
					}
					glPopMatrix();
			
					// do transformation for forarm in current transformation matrix (mtrt or f)
					glTranslatef(1.0f, 0.0f, 0.0f);
					glRotatef(344.0f + 401.0f, 0.0f, 0.0f, 1.0f);
					glTranslatef(1.0f + 0.839f + -0.120f, 0.0f + -0.165f + 0.165f, 0.0f + 0.465f);	

					glPushMatrix();
					{
						glScalef(2.0f, 0.5f, 1.0f);
						glRotatef(245.0f, 1.0, 0.0f, 0.0f);

						// Draw the forarm
						glColor3f(0.8f, 0.6f, 0.4f);
						gluSphere(quadric, 1.1f, 10, 10);

					}
					glPopMatrix();

					glBindTexture(GL_TEXTURE_2D, 0);
				}
				glPopMatrix();
			}
			glPopMatrix();
			glDisable(GL_LIGHTING);
		}
		glPopMatrix();
	}
	if(hanumanEatingScenePart == 3)
	{
		lightAmbient[0] = earthSceneLight;
		lightAmbient[1] = earthSceneLight;
		lightAmbient[2] = earthSceneLight;
		lightAmbient[3] = earthSceneLight;

		lightDiffuse[0] = earthSceneLight;
		lightDiffuse[1] = earthSceneLight;
		lightDiffuse[2] = earthSceneLight;
		lightDiffuse[3] = earthSceneLight;

		materialAmbient[0] = earthSceneLight;
		materialAmbient[1] = earthSceneLight;
		materialAmbient[2] = earthSceneLight;
		materialAmbient[3] = earthSceneLight;

		materialDiffuse[0] = earthSceneLight;
		materialDiffuse[1] = earthSceneLight;
		materialDiffuse[2] = earthSceneLight;
		materialDiffuse[3] = earthSceneLight;
		
		// light related initialization
		glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
		glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
		// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
		glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

		// material related initialization
		glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
		glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
		// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
		glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

		///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			//glEnable(GL_LIGHTING);
			glTranslatef(0.0f, 0.0f, 0.0f);     
			glRotatef(galaxyMovingY, 0.0f, 0.0f, 1.0f);
			// glScalef(scaleX, scaleX, 0.0f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);

		///////////////////////////////////////////// Earth /////////////////////////////////////////////
		glBindTexture(GL_TEXTURE_2D, Texture_Earth);
		gluQuadricTexture(quadric, GL_TRUE);

		glPushMatrix();
		{
			glEnable(GL_LIGHTING);
			glTranslatef(1.30f + objX, -1.50f, -9.25f + objZ);
			glRotatef(246.0f, 1, 1, 1);
			glRotatef(rotate, 0, 0, 1);
			gluSphere(quadric, 6.0f, 50, 50); // gluSphere() create all needed normals fo
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);	
	} 
	
	if(hanumanEatingScenePart == 6)
	{

	// if(hanumanEatingScenePart == 2) {
		// gluLookAt(
		// 	0.0f, hanumanFallingCameraY, hanumanFallingCameraZ, // eye position
		// 	0.0f, 0.0, 0.0f, // eye center
		// 	0.0f, 1.0f, 0.0f // up vector
		// );
	// }
	// gluLookAt(
	// 	eyeX, eyeY, eyeZ, // eye position
	// 	0.0f, 0.0, -10.0f, // eye center
	// 	0.0f, 1.0f, 0.0f // up vector
	// );

	// glTranslatef(0.0f, 0.0f, -50.0f);

	lightAmbient[0] = hanumanEatingSceneLight;
	lightAmbient[1] = hanumanEatingSceneLight;
	lightAmbient[2] = hanumanEatingSceneLight;
	lightAmbient[3] = hanumanEatingSceneLight;

	lightDiffuse[0] = hanumanEatingSceneLight;
	lightDiffuse[1] = hanumanEatingSceneLight;
	lightDiffuse[2] = hanumanEatingSceneLight;
	lightDiffuse[3] = hanumanEatingSceneLight;

	materialAmbient[0] = hanumanEatingSceneLight;
	materialAmbient[1] = hanumanEatingSceneLight;
	materialAmbient[2] = hanumanEatingSceneLight;
	materialAmbient[3] = hanumanEatingSceneLight;

	materialDiffuse[0] = hanumanEatingSceneLight;
	materialDiffuse[1] = hanumanEatingSceneLight;
	materialDiffuse[2] = hanumanEatingSceneLight;
	materialDiffuse[3] = hanumanEatingSceneLight;
	
	// light related initialization
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	// material related initialization
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	//glDisable(GL_LIGHTING);
	///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
	gluQuadricTexture(quadric, GL_TRUE);
	
	glPushMatrix();
	{
		//glEnable(GL_LIGHTING);
		glTranslatef(0.0f, 0.0f, 0.0f);     
		glRotatef(galaxyMovingY, 0.0f, 0.0f, 1.0f);
		// glScalef(scaleX, scaleX, 0.0f);
		// glTranslatef(objX, objY, 0.0f);
		gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
	}	
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// Flying hanuman
	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	glBindTexture(GL_TEXTURE_2D, Texture_FlyingHanuman);

	glPushMatrix();
	{
		glTranslatef(0.70f + -0.1 + babyHanumanFlyingX, 0.250f, -3.00f + -1.85 + babyHanumanFlyingZ);
		// glScalef(scaleX, scaleX, scaleX);
		glRotatef(-9.00f, 0.0, 0, 1);
		glBegin(GL_QUADS);
		glColor3f(1.0f, 1.0f, 1.0f);
		// Right top
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);

		// Left top
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		// Left bottom
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		// Right bottom
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

		glEnd();
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glDisable(GL_BLEND);

	///////////////////////////////////////////// Sun /////////////////////////////////////////////
	glPushMatrix();
	{
		glBindTexture(GL_TEXTURE_2D, Texture_Sun);
		gluQuadricTexture(quadric, GL_TRUE);
		
		glPushMatrix();
		{
			//glEnable(GL_LIGHTING);
			glTranslatef(-0.30f + translateSunX, 0.00f, -1.50f + translateSunZ);
			glScalef(0.20f, 0.20f, 0.20f);
			// glTranslatef(objX, objY, 0.0f);
			gluSphere(quadric, 1.0f, 50, 50); // gluSphere() create all needed normals

			// Sun toppings
			glEnable(GL_BLEND);

			glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

			glBindTexture(GL_TEXTURE_2D, Texture_SunToppings);

			glPushMatrix();
			{
				glTranslatef(-0.05, 0.00, 0.00);
				glScalef(1.35, 1.35, 1.35);
				glRotatef(rotateSunToppings, 0, 0, 1);
				glBegin(GL_QUADS);
				glColor3f(1.0f, 1.0f, 1.0f);
				// Right top
				glTexCoord2f(1.0f, 1.0f);
				glVertex3f(1.0f, 1.0f, 0.0f);

				// Left top
				glTexCoord2f(0.0f, 1.0f);
				glVertex3f(-1.0f, 1.0f, 0.0f);

				// Left bottom
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(-1.0f, -1.0f, 0.0f);

				// Right bottom
				glTexCoord2f(1.0f, 0.0f);
				glVertex3f(1.0f, -1.0f, 0.0f);

				glEnd();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.05, 0.00, 0.00);
				glScalef(1.35, 1.35, 1.35);
				glRotatef(-rotateSunToppings, 0, 0, 1);
				glBegin(GL_QUADS);
				glColor3f(1.0f, 1.0f, 1.0f);
				// Right top
				glTexCoord2f(1.0f, 1.0f);
				glVertex3f(1.0f, 1.0f, 0.0f);

				// Left top
				glTexCoord2f(0.0f, 1.0f);
				glVertex3f(-1.0f, 1.0f, 0.0f);

				// Left bottom
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(-1.0f, -1.0f, 0.0f);

				// Right bottom
				glTexCoord2f(1.0f, 0.0f);
				glVertex3f(1.0f, -1.0f, 0.0f);

				glEnd();
			}
			glPopMatrix();

			glPushMatrix();
			{
				glTranslatef(-0.05, 0.00, 0.00);
				glScalef(1.35, 1.35, 1.35);
				glRotatef(rotateSunToppings, 0, 0, 1);
				glBegin(GL_QUADS);
				glColor3f(1.0f, 1.0f, 1.0f);
				// Right top
				glTexCoord2f(1.0f, 1.0f);
				glVertex3f(1.0f, 1.0f, 0.0f);

				// Left top
				glTexCoord2f(0.0f, 1.0f);
				glVertex3f(-1.0f, 1.0f, 0.0f);

				// Left bottom
				glTexCoord2f(0.0f, 0.0f);
				glVertex3f(-1.0f, -1.0f, 0.0f);

				// Right bottom
				glTexCoord2f(1.0f, 0.0f);
				glVertex3f(1.0f, -1.0f, 0.0f);

				glEnd();
			}
			glPopMatrix();
			glBindTexture(GL_TEXTURE_2D, 0);

			glDisable(GL_BLEND);
		}	
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);
	}	
	glPopMatrix();

/*
	if(hanumanEatingScenePart == 1)
	{
			glPushMatrix();
			{
				// Step 2: Rotate around the x-axis to perform the rotation

				// glRotatef(rotate, 1.0f, 0.0f, 0.0f);
				// glTranslatef(1.0f, 0.0f, 1.0f);
				///////////////////////////////////////////// Left hand /////////////////////////////////////////////
				// set polygon mode to filled (although filled polygon is default already hence not needed)
				glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
				glPushMatrix();
				{
					// do initial translation for better visibility
					glTranslatef(-6.074 + -0.645f, -3.885 + -1.184f, -12.0f);	
					
					glRotatef(302.0f, 1.0f, 0.0f, 0.0f);
					glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
					//glRotatef(rotate, 0.0f, 0.0f, 1.0f);
					// push this matrix
					glPushMatrix();
					{
						// do transformation for arm
						// z-axis is in the direction of x-axis because of problem in glu library
						glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
						glRotatef(-handsMovement, 0.0f, 1.0f, 0.0f);

						glTranslatef(1.0f, 0.0f, 0.0f);
						glScalef(1.5f, 1.0f, 1.0f);

						glBindTexture(GL_TEXTURE_2D, Texture_BalHanumanBiceps);
						gluQuadricTexture(quadric, GL_TRUE);

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);
							glRotatef(338.0f, 1.0f, 0.0f, 0.0f);

							// draw arm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.2f, 10, 10);

							// pop the matrix to come back where arm ended
						}
						glPopMatrix();

						// do transformation for forarm in current transformation matrix (mtrt or f)
						glTranslatef(1.0f, 0.0f, 0.0f);
						glRotatef(344.0f, 0.0f, 0.0f, 1.0f);
						glTranslatef(1.0f + 0.839f, 0.0f + -0.165f, 0.0f + 0.465f);	

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);

							// Draw the forarm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.1f, 10, 10);

						}
						glPopMatrix();

						// do transformation for hand in current transformation matrix (mtrt or f)
						glTranslatef(1.0f + -0.045f, 0.0f + 0.030f, 0.0f + 0.0f);
						glRotatef(344.0f, 0.0f, 0.0f, 1.0f);
						glTranslatef(0.35f, 0.0f, 0.0f);

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);

							// Draw the forarm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.0f, 10, 10);
						}
						glPopMatrix();
						glBindTexture(GL_TEXTURE_2D, 0);
					}
					glPopMatrix();
				}
				glPopMatrix();

				///////////////////////////////////////////// Right hand /////////////////////////////////////////////

				glPushMatrix();
				{
					// do initial translation for better visibility
					glTranslatef(-6.074 + -0.645f + 14.010 + -0.794, -3.885 + -1.184f + -0.135 + 0.300, -12.0f);	
					
					glRotatef(302.0f, 1.0f, 0.0f, 0.0f);
					glRotatef(90.0f, 0.0f, 0.0f, 1.0f);
					
					//glRotatef(rotate, 0.0f, 0.0f, 1.0f);
					// push this matrix
					glPushMatrix();
					{

						// do transformation for arm
						// z-axis is in the direction of x-axis because of problem in glu library
						glRotatef((GLfloat)shoulder, 0.0f, 0.0f, 1.0f);
						glRotatef(-handsMovement, 0.0f, 1.0f, 0.0f);
						glTranslatef(1.0f, 0.0f, 0.0f);
						glScalef(1.5f, 1.0f, 1.0f);

						glBindTexture(GL_TEXTURE_2D, Texture_BalHanumanBiceps);
						gluQuadricTexture(quadric, GL_TRUE);

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);
							glRotatef(338.0f, 1.0f, 0.0f, 0.0f);
							glRotatef(1361.0f, 1.0, 0.0f, 0.0f);

							// draw arm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.2f, 10, 10);

							// pop the matrix to come back where arm ended
						}
						glPopMatrix();
				
						// do transformation for forarm in current transformation matrix (mtrt or f)
						glTranslatef(1.0f, 0.0f, 0.0f);
						glRotatef(344.0f + 401.0f, 0.0f, 0.0f, 1.0f);
						glTranslatef(1.0f + 0.839f + -0.120f, 0.0f + -0.165f + 0.165f, 0.0f + 0.465f);	

						glPushMatrix();
						{
							glScalef(2.0f, 0.5f, 1.0f);
							glRotatef(245.0f, 1.0, 0.0f, 0.0f);

							// Draw the forarm
							glColor3f(0.8f, 0.6f, 0.4f);
							gluSphere(quadric, 1.1f, 10, 10);

						}
						glPopMatrix();

						glBindTexture(GL_TEXTURE_2D, 0);
					}
					glPopMatrix();
				}
				glPopMatrix();
				glDisable(GL_LIGHTING);
			}
			glPopMatrix();
		}
	}
}*/

void drawBalHanumanFlyingScene() 
{
	glTranslatef(0.0f, 0.0f, -5.0f);

	gluLookAt(
		moveCameraXInGalaxy, moveCameraYInGalaxy, moveCameraZInGalaxy, // eye position
		moveCameraCenterXInGalaxy, moveCameraCenterYInGalaxy, moveCameraCenterZInGalaxy, // eye center
		0.0f, 1.0f, 0.0f // up vector
	);

	float hanumanFlyingSceneLight = 1.0f;

	lightAmbient[0] = hanumanFlyingSceneLight;
	lightAmbient[1] = hanumanFlyingSceneLight;
	lightAmbient[2] = hanumanFlyingSceneLight;
	lightAmbient[3] = hanumanFlyingSceneLight;

	lightDiffuse[0] = hanumanFlyingSceneLight;
	lightDiffuse[1] = hanumanFlyingSceneLight;
	lightDiffuse[2] = hanumanFlyingSceneLight;
	lightDiffuse[3] = hanumanFlyingSceneLight;

	materialAmbient[0] = hanumanFlyingSceneLight;
	materialAmbient[1] = hanumanEatingSceneLight;
	materialAmbient[2] = hanumanFlyingSceneLight;
	materialAmbient[3] = hanumanFlyingSceneLight;

	materialDiffuse[0] = hanumanFlyingSceneLight;
	materialDiffuse[1] = hanumanFlyingSceneLight;
	materialDiffuse[2] = hanumanFlyingSceneLight;
	materialDiffuse[3] = hanumanFlyingSceneLight;
	
	// light related initialization
	glLightfv(GL_LIGHT0, GL_AMBIENT, lightAmbient);
	glLightfv(GL_LIGHT0, GL_DIFFUSE, lightDiffuse);
	// glLightfv(GL_LIGHT0, GL_SPECULAR, lightSpecular);
	glLightfv(GL_LIGHT0, GL_POSITION, lightPosition);

	// material related initialization
	glMaterialfv(GL_FRONT, GL_AMBIENT, materialAmbient);
	glMaterialfv(GL_FRONT, GL_DIFFUSE, materialDiffuse);
	// glMaterialfv(GL_FRONT, GL_SPECULAR, materialSpecular);
	glMaterialfv(GL_FRONT, GL_SHININESS, materialShininess);

	///////////////////////////////////////////// Galaxy /////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, Texture_Galaxy);
	gluQuadricTexture(quadric, GL_TRUE);
	
	glPushMatrix();
	{
		glEnable(GL_LIGHTING);
		glTranslatef(0.0f, 0.0f, 0.0f);     
		glRotatef(galaxyMovingY * 1.5f, 0.0f, 0.0f, 1.0f);
		// glScalef(scaleX, scaleX, 0.0f);
		// glTranslatef(objX, objY, 0.0f);
		gluSphere(quadric, 50.0f, 50, 50); // gluSphere() create all needed normals
	}	
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	///////////////////////////////////////////// Earth /////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, Texture_Earth);
	gluQuadricTexture(quadric, GL_TRUE);

	glPushMatrix();
	{
		glTranslatef(-2.445f, -5.609f, 0.0f);
		gluSphere(quadric, 4.0f, 50, 50); // gluSphere() create all needed normals fo
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE);

	///////////////////////////////////////////// Another Earth /////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, Texture_AnotherEarth);
	gluQuadricTexture(quadric, GL_TRUE);

	glPushMatrix();
	{
		// glEnable(GL_LIGHTING);
		glColor4f(1.0f, 1.0f, 1.0f, 0.1f);
		glTranslatef(-2.445f, -5.609f, 0.0f);
		gluSphere(quadric, 4.02f, 50, 50); // gluSphere() create all needed normals fo
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	glDisable(GL_BLEND);

	glEnable(GL_BLEND);

	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

	///////////////////////////////////////////// Bal-hanuman Flying /////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, Texture_BalHanuman);

	glPushMatrix();
	{
		glEnable(GL_LIGHTING);
		//glTranslatef(objX, objY, 0.0f);
		//glScalef(scaleX, scaleX, 0);
		glTranslatef(-1.139f + balHanumanFlyX, -0.779f + balHanumanFlyY, 0.0f);
		glScalef(0.180f, 0.180f, 0);
		glRotatef(347.0f, 0.0f, 0.0f, 1.0f);
		glBegin(GL_QUADS);

		// Right top
		glTexCoord2f(1.0f, 1.0f);
		glVertex3f(1.0f, 1.0f, 0.0f);

		// Left top
		glTexCoord2f(0.0f, 1.0f);
		glVertex3f(-1.0f, 1.0f, 0.0f);

		// Left bottom
		glTexCoord2f(0.0f, 0.0f);
		glVertex3f(-1.0f, -1.0f, 0.0f);

		// Right bottom
		glTexCoord2f(1.0f, 0.0f);
		glVertex3f(1.0f, -1.0f, 0.0f);

		glEnd();
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);

	// glDisable(GL_BLEND);

	///////////////////////////////////////////// Sun /////////////////////////////////////////////
	glBindTexture(GL_TEXTURE_2D, Texture_Sun);
	gluQuadricTexture(quadric, GL_TRUE);

	glPushMatrix();
	{
		// glDisable(GL_LIGHTING);
		// glTranslatef(4.380f, 1.534f, 0.0f);
		// glTranslatef(3.885f, 2.955f, 0.0f);
		glTranslatef(5.60f, 5.60f, -3.14f);
		glScalef(2.25f, 2.25f, 2.25f);
		gluSphere(quadric, 1.8f, 50, 50); // gluSphere() create all needed normals

		// Sun toppings
		glBindTexture(GL_TEXTURE_2D, Texture_SunToppings);

		glPushMatrix();
		{
			glTranslatef(0.25f, 0.55f, -0.55f);
			glScalef(2.84f, 2.84f, 0);
			glRotatef(rotateSunToppings, 0, 0, 1);
			glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);
			// Right top
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			// Left top
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			// Left bottom
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			// Right bottom
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glEnd();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.25f, 0.55f, -0.65f);
			glScalef(2.84f, 2.84f, 0);
			glRotatef(-rotateSunToppings, 0, 0, 1);
			glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);
			// Right top
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			// Left top
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			// Left bottom
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			// Right bottom
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glEnd();
		}
		glPopMatrix();

		glPushMatrix();
		{
			glTranslatef(0.25f, 0.55f, -0.75f);
			glScalef(2.84f, 2.84f, 0);
			glRotatef(rotateSunToppings, 0, 0, 1);
			glBegin(GL_QUADS);
			glColor3f(1.0f, 1.0f, 1.0f);
			// Right top
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			// Left top
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			// Left bottom
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			// Right bottom
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glEnd();
		}
		glPopMatrix();
		glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();
	glBindTexture(GL_TEXTURE_2D, 0);
	glDisable(GL_BLEND);

	if(Global_Time <= HanumanFlyingTowardsSun + 18)
	{
		glDisable(GL_LIGHTING);
		glEnable(GL_BLEND);

		glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);

		glColor4f(1.0f, 1.0f, 1.0f, demoTitleFadeInAlpha);

		glBindTexture(GL_TEXTURE_2D, Texture_DemoTitle);

		float fadeValue = 1.0f;

		if(demoTitleFadeInAlpha <= 1.0f)
		{
			demoTitleFadeInAlpha += 0.003f;
			fadeValue = demoTitleFadeInAlpha;
		} 
		else if(Global_Time > HanumanFlyingTowardsSun + 14)
		{
			demoTitleFadeOutAlpha -= 0.007f;
			fadeValue = demoTitleFadeOutAlpha;
		}

		glColor4f(1.0f, 1.0f, 1.0f, fadeValue);

		glPushMatrix();
		// glTranslatef(0.0f, 2.0f, -1.0f);
		{
			glTranslatef(0.0f, 0.0f + -0.15f, 0.5f);
			//glScalef(scaleX, scaleX, 0);
			glScalef(3 * (0.9f + 0.10f), 3 * (0.8f + -0.15f), 0);
			
			glBegin(GL_QUADS);

			// glColor4f(1.0f, 1.0f, 1.0f, 0);
			// glColor3f(1.0f, 1.0f, 1.0f);
			// Right top
			glTexCoord2f(1.0f, 1.0f);
			glVertex3f(1.0f, 1.0f, 0.0f);

			// Left top
			glTexCoord2f(0.0f, 1.0f);
			glVertex3f(-1.0f, 1.0f, 0.0f);

			// Left bottom
			glTexCoord2f(0.0f, 0.0f);
			glVertex3f(-1.0f, -1.0f, 0.0f);

			// Right bottom
			glTexCoord2f(1.0f, 0.0f);
			glVertex3f(1.0f, -1.0f, 0.0f);

			glEnd();
		}
		glPopMatrix();

		glDisable(GL_BLEND);
		glEnable(GL_LIGHTING);
	}
}

BOOL LoadPNGTexture(GLuint* texture, char* file)
{
	// variable declarations
	int w, h;
	int channels_in_file;
	BOOL bResult = FALSE;

	// load the image using stbi library
	unsigned char *image = stbi_load(file, &w, &h, &channels_in_file, STBI_rgb_alpha);
	
	// code
	if (image)
	{
		bResult = TRUE;
		glPixelStorei(GL_UNPACK_ALIGNMENT, 4);
		glGenTextures(1, texture);
		glBindTexture(GL_TEXTURE_2D, *texture);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		
		// create the texture
		if (channels_in_file == 3)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, w, h, 0, GL_RGB, GL_UNSIGNED_BYTE, image);
			fprintf(gpFile, "Loading a non-transparent image!!!\n");
		}
		else if (channels_in_file == 4)
		{
			glTexImage2D(GL_TEXTURE_2D, 0, GL_RGBA, w, h, 0, GL_RGBA, GL_UNSIGNED_BYTE, image);
			fprintf(gpFile, "Loading a transparent image!!!\n");
		}

		// unbind
		glBindTexture(GL_TEXTURE_2D, 0);

		// free the image
		stbi_image_free(image);
	}
	else
	{
		fprintf(gpFile, "\n%s - %s", file, stbi_failure_reason());
	}

	return bResult;
}

void update(void)
{
	// code

	palaceCounter = 0;

	if(present == HanumanFlyingTowardsSun)
	{
		if(balHanumanFlyX < 2.72f) 
		{
			balHanumanFlyX += 0.01f - 0.01f / 1.26;
		}

		if(balHanumanFlyY < 1.824f) 
		{
			balHanumanFlyY += 0.0065f - 0.0065f / 1.26;
		}

		float inc = 1.20f;
	
		if(moveCameraXInGalaxy > 0)
		{
			moveCameraXInGalaxy -=0.016 - 0.016 /inc;
		}
		if(moveCameraYInGalaxy < 0)
		{
			moveCameraYInGalaxy +=0.043 - 0.043/inc;
		}
		if(moveCameraZInGalaxy < 0.1)
		{
			moveCameraZInGalaxy +=0.0209 - 0.0209 / inc;
		}

		if(moveCameraCenterXInGalaxy < 0)
		{
			moveCameraCenterXInGalaxy +=0.031 - 0.031 / inc;
		}

		if(moveCameraCenterZInGalaxy > 0)
		{
			moveCameraCenterZInGalaxy -=0.0005 - 0.0005 / inc;
		}

		if(rotateSunToppings < 360.0f)
		{
			rotateSunToppings += 0.08f;
		}
		else
		{
			rotateSunToppings -= 360.0f;
		}
	}

	if(present == HanumanEatingSun)
	{
		float timeAdjustment = 1.8f;

		if(hanumanEatingScenePart == 1)
		{
				if(translateSunX > 0)
				{
					translateSunX -= 0.00167f - 0.00167f / 1.275;
				}

				if(translateSunZ > 0)
				{
					translateSunZ -= 0.0067f - 0.0067f / 1.275;
				}
				else
				{
					hanumanEatingScenePart = 2;
				}
				
				if(rotateSunToppings < 360.0f)
				{
					rotateSunToppings += 0.3f;
				}
				else
				{
					rotateSunToppings -= 360.0f;
				}

				if(babyHanumanFlyingX > -0.40)
				{
					babyHanumanFlyingX -= 0.0016 - 0.0016 / 1.45;
				}
				if(babyHanumanFlyingZ < 1.30)
				{
					babyHanumanFlyingZ += 0.0052 - 0.0052 / 1.45;
				}
		}

		if(hanumanEatingScenePart == 2)
		{
			if(rotateSunToppings < 360.0f)
			{
				rotateSunToppings += 0.3f;
			}
			else
			{
				rotateSunToppings -= 360.0f;
			}

			if(hanumanEatingScenePart == 2 && sunTranslateZ < 1.400) 
			{
				sunTranslateZ += 0.005 - 0.005 / 2.5f;
			}
			else if(hanumanEatingScenePart == 2)
			{
				hanumanEatingScenePart = 3;
			}

			if(handsMovement < 44.0) 
			{
				handsMovement += 0.25 - 0.25 / 2.5f;
			}

			if(hanumanEatingScenePart == 2 && hanumanEatingSceneLight > 0.15f) 
			{
				hanumanEatingSceneLight -= 0.003f - 0.003f / 2.5f;
			} 
		}

		if(hanumanEatingScenePart == 3)
		{
			if(earthSceneLight > -0.1f) 
			{
				earthSceneLight -= 0.003f;
			} 

			if(rotateBlackEarth < 360.0f)
			{
				rotateBlackEarth += 0.02f;
			}
			else
			{
				rotateBlackEarth -= 360.0f;
			}
		}

		if(hanumanEatingScenePart > 3)
		{
			hanumanEatingScenePart = 2;

			if(hanumanEatingSceneLight < 1.0f) 
			{
				hanumanEatingSceneLight += 0.01f - 0.01f / timeAdjustment;
			} 

			if(sunTranslateY < 40) 
			{
				sunTranslateY += 0.150f - 0.150f / timeAdjustment;
			}
			else
			{
				hanumanEatingScenePart = 3;
			}
		}
	}

	if(present == HanumanLiftingSanjivani && cameraMovementFromMountain < 14.73f)
	{
		cameraMovementFromMountain += 0.005f;

		birdRotation += 2.0f;
	}

	if(present == LankaDahan)
	{
		if (t < 1.0f)
		{
			t += 0.0005f;
		}
		else
		{
			t = 0.0;
		}
	}

	galaxyMovingY += 0.03f;
}

void uninitialize(void)
{
	// function declarations
	void ToggleFullScreen(void);

	// code
	glDisable(GL_LIGHTING);

	if (Texture_Outro5)
	{
		glDeleteTextures(1, &Texture_Outro5);
		Texture_Outro5 = 0;
	}

	if (Texture_Outro4)
	{
		glDeleteTextures(1, &Texture_Outro4);
		Texture_Outro4 = 0;
	}

	if (Texture_Outro3)
	{
		glDeleteTextures(1, &Texture_Outro3);
		Texture_Outro3 = 0;
	}

	if (Texture_Outro2)
	{
		glDeleteTextures(1, &Texture_Outro2);
		Texture_Outro2 = 0;
	}

	if (Texture_Outro1)
	{
		glDeleteTextures(1, &Texture_Outro1);
		Texture_Outro1 = 0;
	}

	if (Texture_Intro1)
	{
		glDeleteTextures(1, &Texture_Intro1);
		Texture_Intro1 = 0;
	}

	if (Texture_DemoTitle)
	{
		glDeleteTextures(1, &Texture_DemoTitle);
		Texture_DemoTitle = 0;
	}

	if (Texture_SunToppings)
	{
		glDeleteTextures(1, &Texture_SunToppings);
		Texture_SunToppings = 0;
	}

	if (Texture_IndraVajra)
	{
		glDeleteTextures(1, &Texture_IndraVajra);
		Texture_IndraVajra = 0;
	}

	if (Texture_FlyingHanuman)
	{
		glDeleteTextures(1, &Texture_FlyingHanuman);
		Texture_FlyingHanuman = 0;
	}

	if (Texture_HanumanHand)
	{
		glDeleteTextures(1, &Texture_HanumanHand);
		Texture_HanumanHand = 0;
	}

	if (Texture_SanjivaniMountain)
	{
		glDeleteTextures(1, &Texture_SanjivaniMountain);
		Texture_SanjivaniMountain = 0;
	}

	if (Texture_Fire)
	{
		glDeleteTextures(1, &Texture_Fire);
		Texture_Fire = 0;
	}

	if (Texture_Base_Temple5)
	{
		glDeleteTextures(1, &Texture_Base_Temple5);
		Texture_Base_Temple5 = 0;
	}

	if (Texture_Mid_Temple5)
	{
		glDeleteTextures(1, &Texture_Mid_Temple5);
		Texture_Mid_Temple5 = 0;
	}

	if (Texture_Top_Temple5)
	{
		glDeleteTextures(1, &Texture_Top_Temple5);
		Texture_Top_Temple5 = 0;
	}

	if (Texture_Base_Temple4)
	{
		glDeleteTextures(1, &Texture_Base_Temple4);
		Texture_Base_Temple4 = 0;
	}

	if (Texture_Mid_Temple4)
	{
		glDeleteTextures(1, &Texture_Mid_Temple4);
		Texture_Mid_Temple4 = 0;
	}

	if (Texture_Base_Temple3)
	{
		glDeleteTextures(1, &Texture_Base_Temple3);
		Texture_Base_Temple3 = 0;
	}

	if (Texture_Mid_Temple3)
	{
		glDeleteTextures(1, &Texture_Mid_Temple3);
		Texture_Mid_Temple3 = 0;
	}

	if (Texture_Top_Temple3)
	{
		glDeleteTextures(1, &Texture_Top_Temple3);
		Texture_Top_Temple3 = 0;
	}

	if (Texture_Base_Temple2)
	{
		glDeleteTextures(1, &Texture_Base_Temple2);
		Texture_Base_Temple2 = 0;
	}

	if (Texture_Mid_Temple2)
	{
		glDeleteTextures(1, &Texture_Mid_Temple2);
		Texture_Mid_Temple2 = 0;
	}

	if (Texture_Top_Temple2)
	{
		glDeleteTextures(1, &Texture_Top_Temple2);
		Texture_Top_Temple2 = 0;
	}

	if (Texture_Base_Temple1)
	{
		glDeleteTextures(1, &Texture_Base_Temple1);
		Texture_Base_Temple1 = 0;
	}

	if (Texture_Top_Temple1)
	{
		glDeleteTextures(1, &Texture_Top_Temple1);
		Texture_Top_Temple1 = 0;
	}

	if (Texture_HanumanFace)
	{
		glDeleteTextures(1, &Texture_HanumanFace);
		Texture_HanumanFace = 0;
	}

	if (Texture_BalHanumanBiceps)
	{
		glDeleteTextures(1, &Texture_BalHanumanBiceps);
		Texture_BalHanumanBiceps = 0;
	}

	if (Texture_BalHanuman)
	{
		glDeleteTextures(1, &Texture_BalHanuman);
		Texture_BalHanuman = 0;
	}

	if (Texture_Sun)
	{
		glDeleteTextures(1, &Texture_Sun);
		Texture_Sun = 0;
	}

	if (Texture_AnotherEarth)
	{
		glDeleteTextures(1, &Texture_AnotherEarth);
		Texture_AnotherEarth = 0;
	}

	if (Texture_Earth)
	{
		glDeleteTextures(1, &Texture_Earth);
		Texture_Earth = 0;
	}

	if (Texture_Galaxy)
	{
		glDeleteTextures(1, &Texture_Galaxy);
		Texture_Galaxy = 0;
	}

	if (quadric)
	{
		gluDeleteQuadric(quadric);
		quadric = NULL;
	}

	// if application is exiting in full screen
	if (gbFullScreen == TRUE)
	{
		ToggleFullScreen();
		gbFullScreen = FALSE;
	}

	// Make the hdc as current context
	if (wglGetCurrentContext() == ghrc)
	{
		wglMakeCurrent(NULL, NULL);
	}

	// Delete rendering context
	if (ghrc)
	{
		wglDeleteContext(ghrc);
		ghrc = NULL;
	}

	// Release the hdc
	if (ghdc)
	{
		ReleaseDC(ghwnd, ghdc);
		ghdc = NULL;
	}

	// destroy window
	if (ghwnd)
	{
		DestroyWindow(ghwnd);
		ghwnd = NULL;
	}

	// close the log file
	if (gpFile)
	{
		fprintf(gpFile, "Program ended successfully...\n");
		fclose(gpFile);
		gpFile = NULL;
	}
}

void colorVertices(float red, float green, float blue, float alpha)
{
	float r = red / 255.0f;
	float g = green / 255.0f;
	float b = blue / 255.0f;
	float a = alpha / 255.0f;
	glColor4f(r, g, b, a);
}

BOOL LoadHeightMapData(char* file)
{
	int w;
	int h;
	int channels_in_file;
	heightMapData = stbi_load(file, &w, &h, &channels_in_file, 0);

	if (heightMapData == NULL)
	{
		fprintf(gpFile, "File %s loading failed", file);
		return FALSE;
	}

	return TRUE;
}

void InitializeTerrainCoords()
{
	int heightMultiplier = 2;

	for (int z = 0; z < TERRAIN_MAP_Z; z++)
	{
		for (int x = 0; x < TERRAIN_MAP_X; x++)
		{
			terrainHeightMap[x][z][0] = float(x) * MAP_SCALE;
			terrainHeightMap[x][z][1] = heightMapData[(x + z * TERRAIN_MAP_Z) * 3] * heightMultiplier;
			terrainHeightMap[x][z][2] = -float(z) * MAP_SCALE;
		}
	}
}

void drawTerrain()
{
	glPushMatrix();
	{
		// glBindTexture(GL_TEXTURE_2D, texture_grass);
		// glTexEnvf(GL_TEXTURE_ENV, GL_TEXTURE_ENV_MODE, GL_MODULATE);
		for (int z = 0; z < (TERRAIN_MAP_Z - 1); z++)
		{
			glBegin(GL_TRIANGLE_STRIP);
			for (int x = 0; x < (TERRAIN_MAP_X - 1); x++)
			{
				float scaledHeight = terrainHeightMap[x][z][1] / MAP_SCALE;
				float nextScaledHeight = terrainHeightMap[x][z + 1][1] / MAP_SCALE;
				float color = 0.5f + 0.5f * scaledHeight / MAX_HEIGHT;
				float nextColor = 0.5f + 0.5f * nextScaledHeight / MAX_HEIGHT;

				glColor3f(color, color, color);
				glTexCoord2f(0, 0);
				glVertex3f(terrainHeightMap[x][z][0], terrainHeightMap[x][z][1], terrainHeightMap[x][z][2]);
				glTexCoord2f(1, 0);
				glVertex3f(terrainHeightMap[x + 1][z][0], terrainHeightMap[x + 1][z][1], terrainHeightMap[x + 1][z][2]);
			
				glColor3f(nextColor, nextColor, nextColor);
				glTexCoord2f(0, 1);
				glVertex3f(terrainHeightMap[x][z + 1][0], terrainHeightMap[x][z + 1][1], terrainHeightMap[x][z + 1][2]);
				glTexCoord2f(1, 1);
				glVertex3f(terrainHeightMap[x + 1][z + 1][0], terrainHeightMap[x + 1][z + 1][1], terrainHeightMap[x + 1][z + 1][2]);
			}
			glEnd();
		}
		// glBindTexture(GL_TEXTURE_2D, 0);
	}
	glPopMatrix();
}


void drawCube() 
{
	// cube
	glTranslatef(0.0f, 0.0f, -6.0f);
	glScalef(2000.0f, 2000.0f, 2000.0f);

	// front face

	glBindTexture(GL_TEXTURE_2D, Texture_CubeMap[5]);

	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f); // right top

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f); // left top

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

	// right face

	glBindTexture(GL_TEXTURE_2D, Texture_CubeMap[3]);

	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f); // right top

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, 1.0f); // left top

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); // left bottom

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // right bottom

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

	// back face

	glBindTexture(GL_TEXTURE_2D, Texture_CubeMap[2]);

	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f); // right top

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f); // left top

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // left bottom

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // right bottom

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

	// left face

	glBindTexture(GL_TEXTURE_2D, Texture_CubeMap[0]);

	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f); // right top

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // left bottom

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); // right bottom

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

	// top face

	glBindTexture(GL_TEXTURE_2D, Texture_CubeMap[4]);

	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, 1.0f, -1.0f); // right top

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, 1.0f, -1.0f); // left top

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, 1.0f, 1.0f); // left bottom

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, 1.0f, 1.0f); // right bottom

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);

	// bottom face

	glBindTexture(GL_TEXTURE_2D, Texture_CubeMap[1]);

	glBegin(GL_QUADS);
	
	glTexCoord2f(0.0f, 0.0f);
	glVertex3f(1.0f, -1.0f, -1.0f); // right top

	glTexCoord2f(1.0f, 0.0f);
	glVertex3f(-1.0f, -1.0f, -1.0f); // left top

	glTexCoord2f(1.0f, 1.0f);
	glVertex3f(-1.0f, -1.0f, 1.0f); // left bottom

	glTexCoord2f(0.0f, 1.0f);
	glVertex3f(1.0f, -1.0f, 1.0f); // right bottom

	glEnd();

	glBindTexture(GL_TEXTURE_2D, 0);
}

GLuint createTexture2D(const char *filePath)
{
	int width, height, channel;
	// stbi_set_flip_vertically_on_load(TRUE);

	unsigned char *data = stbi_load(filePath, &width, &height, &channel, 0);

	if(data == NULL) 
	{
		fprintf(gpFile, "Failed to load texture %s\n", filePath);
		return -1;
	}

	GLenum format = GL_RGBA;

	if(channel == STBI_grey)
		format = GL_RED;
	else if(channel == STBI_rgb)
		format = GL_RGB;
	else if(channel == STBI_rgb_alpha)
		format = GL_RGBA;

	GLuint texture;

	glPixelStorei(GL_UNPACK_ALIGNMENT, 1);

	glGenTextures(1, &texture);
	glBindTexture(GL_TEXTURE_2D, texture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);

	glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);

	glBindTexture(GL_TEXTURE_2D, 0);

	stbi_image_free(data);

	return texture;
}

double randomNormal (double mu, double sigma)
{
  double sum = 0;
  for(int i = 0; i < 12; i++)
    sum = sum + (double)rand()/RAND_MAX;
  sum -= 6;
  return sigma*sum + mu;
}

void draw_circle(double radius, double xLoc, double yLoc)
{
    double theta = 0;
    double increment = (10*3.14159265)/180; // 10 degrees
    glBegin(GL_POLYGON);
    for(int i=0; i<36; i++)
    {
        float x = radius*cos(theta) + xLoc;
        float y = radius*sin(theta) + yLoc;
        glVertex2f(x,y);
        theta+=increment;
    }
    glEnd();
}

void fracMountain(float x1, float y1, float z1, float x2, float y2, float z2, float x3, float y3, float z3, float x4, float y4, float z4)
{
	double randomNormal (double mu, double sigma);

    double perim = sqrt((x2-x1)*(x2-x1) + (z2-z1)*(z2-z1)) + sqrt((x3-x2)*(x3-x2) + (z3-z2)*(z3-z2))
        + sqrt((x4-x3)*(x4-x3) + (z4-z3)*(z4-z3)) + sqrt((x1-x4)*(x1-x4) + (z1-z4)*(z1-z4));
    if(perim <= tolerance)
    {
        double v1=x2-x1, v2=y2-y1, v3=z2-z1;
        double w1=x3-x2, w2=y3-y2, w3=z3-z2;
        double crossx=v2*w3-w2*v3;
        double crossy=v3*w1-w3*v1;
        double crossz=v1*w2-w1*v2;

        double tolightx=lightPos[0]-x1;
        double tolighty=lightPos[1]-y1;
        double tolightz=lightPos[2]-z1;

        double dot =crossx*tolightx +crossy*tolighty +crossz*tolightz;
        double normc=sqrt(crossx*crossx +crossy*crossy + crossz*crossz);
        double norml = sqrt(tolightx*tolightx + tolighty*tolighty + tolightz*tolightz);

        double lightamt=dot/(normc*norml);
        if(lightamt>1)
            lightamt=1;
        if(lightamt<0)
            lightamt=0.5;

        lightamt=(lightamt*2 +0.15)/2.15;
        glColor3f(lightamt,lightamt,lightamt);
        glBegin(GL_POLYGON);
            glVertex3f(x1,y1,z1);
            glVertex3f(x2,y2,z2);
            glVertex3f(x3,y3,z3);
            glVertex3f(x4,y4,z4);
        glEnd();
    }
    else
    {
        double r1 = (double)randomNormal(0.041, 0.1);
        double r2 = (double)randomNormal(0.0,   0.01);
        double r3 = (double)randomNormal(0.0,   0.01);
        double xmid = ((x1+x2+x3+x4)/4)+r2*perim;
        double ymid = ((y1+y2+y3+y4)/4)+r1*perim;
        if(ymid<0)
            ymid=0;
        double zmid = ((z1+z2+z3+z4)/4)+r3*perim;
        fracMountain(x1,y1,z1,(x1+x2)/2,(y1+y2)/2,(z1+z2)/2,xmid,ymid,zmid,(x1+x4)/2,(y1+y4)/2,(z1+z4)/2);
        fracMountain((x1+x4)/2,(y1+y4)/2,(z1+z4)/2,xmid,ymid,zmid,(x4+x3)/2,(y4+y3)/2,(z4+z3)/2,x4,y4,z4);
        fracMountain((x1+x2)/2,(y1+y2)/2,(z1+z2)/2,x2,y2,z2,(x2+x3)/2,(y2+y3)/2,(z2+z3)/2,xmid,ymid,zmid);
        fracMountain(xmid,ymid,zmid,(x2+x3)/2,(y2+y3)/2,(z2+z3)/2,x3,y3,z3,(x3+x4)/2,(y3+y4)/2,(z3+z4)/2);
    }
}

int TimeCalculation(SYSTEMTIME currentTime, SYSTEMTIME startTime)
{
	// code
	int currentSeconds = currentTime.wMinute * 60 + currentTime.wSecond;
	int startSeconds = startTime.wMinute * 60 + startTime.wSecond;

	return currentSeconds - startSeconds;
}

float de_casteljau(float t, float coefficients[] , float size) {

	float* beta = (float*)malloc(size * sizeof(float));
	if (beta == NULL) {
		
		fprintf(gpFile, "Memory Allocation Failed\n");
		return 0.0f; 
	}

	for (int i = 0; i < size; ++i) {
		beta[i] = coefficients[i];
	}

	for (int j = 1; j < size; j++) {
		for (int k = 0; k < size - j; k++) {
			beta[k] = beta[k] * (1 - t) + beta[k + 1] * t;
		}
	}

	float result = beta[0];
	free(beta);
	return result;
}

float degToRad(float degrees)
{
	return (degrees * M_PI / 180.0);
}
